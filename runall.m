%%%%%%%%%%%%%%%%%%%%
%% STXM spectra classification program
%% ~ runall.m ~
%% $Rev$
%% Sept. 2009, distributed Dec. 2009
%% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%


% $$$ run after 
% $$$ 1) setting paths in runheadr.m
% $$$ 2) setting data directory in userinputs.m

run main_prog_merge
% $$$ run main_prog_merge
run getavespec
run plot_particles
run fit_export_plot_avefit
run export_diamspectra
run fit_plot_pixelfit
run plot_edges_radial
% sudo run fit_export_edgetest_callR.m
