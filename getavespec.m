%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getavespec.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

% $$$ constructs bw image and average spectra for each particle
% $$$ based on carbon regions

clear
run headr;
run userinputs;

run getstacklist; % creates variable 'stacks' in global space

for iter = {stacks.name},
  stackname = strrep(char(iter),'.mat','');
  if ~isempty(strfind(stackname,'.')), continue; end
  load(fullfile(matdir,sprintf('%s.mat',stackname)));
  if ~isempty(strmatch('avespec',fieldnames(S.particles(1)))), 
      continue; 
  end
  for i = 1:length(S.particles),
    disp(S.particles(i).name);
        
    %% --- make mask ---
    bw = [];
    for j = 1:length(S.particles(i).carbon_regions),
      if isempty(bw),
        bw = S.particles(i).carbon_regions(j).bw;
      else,
        bw = bw + S.particles(i).carbon_regions(j).bw;
      end
    end
    S.particles(i).bw = bw;
    
    %% --- make avespec ---
    stackarr = reshape(S.stackarr,[],size(S.stackarr,3));
    bwlong = reshape(bw,[],1);
    S.particles(i).avespec = nanmean(stackarr(find(bwlong),:));
  end
  save(fullfile(matdir,stackname),'S');    
end

