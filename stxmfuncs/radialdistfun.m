%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~radialdistfun.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function radial = radialdistfun(self,refev,bwimg)
sh = shapemetrics;

if isnumeric(refev) & min(size(refev))==1,
  opdens = ...
    self.stackarr(:,:,getElem(abs(self.energy-refev),@min, 2));
elseif isnumeric(refev) & min(size(refev))>1,
  opdens = refev;
else,
  opdens = self.(refev);
end

centre = feval(sh.findcenter,self.xcoord,self.ycoord,opdens, ...
  bwimg);
[x,y] = meshgrid(self.xcoord,self.ycoord);
radialdist = sqrt((x - centre{1}).^2 + ...
  (y - centre{2}).^2);

radial = {radialdist(:),opdens(:),bwimg(:),reshape(bwperim(bwimg),[],1),centre};  

return
