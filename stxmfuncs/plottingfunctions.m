%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~plottingfunctions.m~
% $Rev: 12 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function obj = plottingfunctions()
obj = struct('avespecplot',@avespecplot,...
	     'physmorphimages',@physmorphimages,...
	     'chemmorphimages',@chemmorphimages,...
	     'edgexyplots',@edgexyplots,...
       'Kplots',@Kplots,...
       'failedplots',@failedplots);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function filename = outfile(self,directory,suffix)
filename = fullfile(directory,strcat(self.name,suffix));
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function avespecplot(self,meth,directory,suffix)
if nargin < 3, dosave=0; else, dosave=1; end

if ~exist('RColors','var'),
  load RColors;
end
cfun = loadCfunctions;

normed = feval(cfun.normalizeC,[self.energy',self.avespec']);
extraArgs = getElem({normed(:,1),normed(:,2),meth},cfun.getArgs,3);
if length(extraArgs) > 0,
  ymat = feval(feval(cfun.selectckernel,meth),...
    self.aveccoeffs(3:end)',self.energy',extraArgs);
else,  
  ymat = feval(feval(cfun.selectckernel,meth),...
    self.aveccoeffs(3:end)',self.energy');
end
h = plot(self.energy,ymat);
line(normed(:,1),normed(:,2),'color',RColors.('steelblue'),...
     'linew',1.5)
line(self.energy,sum(ymat)','color',RColors.('orange1'),'linew',2.5)
title(strcat(strrep(self.name,'_','\_'),sprintf('meth%d',meth)));
if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function physmorphimages(self,directory,suffix)
if nargin < 2, dosave = 0; else, dosave=1; end

%% change these to function arguments if desired
im1 = 'bw288';
im2 = 'bw';
refev = 288.85;
evrg = [275,320];

subplot(2,2,1)
myimagesc(self,self.stackarr(:,:,getElem(abs(self.energy-refev),@min,2)));
xlabel('x (\mum)'); ylabel('y (\mum)');
title(sprintf('image at %s eV',num2str(refev)))
subplot(2,2,2);
myimagesc(self,self.totalc);
xlabel('x (\mum)'); ylabel('y (\mum)');
title('total carbon map');
subplot(2,2,3)
myimagesc(self,self.(im1) + self.(im2) );
xlabel('x (\mum)'); ylabel('y (\mum)');
title('thresholded; labeled');  
subplot(2,2,4)
plot(self.energy,self.avespec);
set(gca,'xlim',evrg);
xlabel('energy (eV)'); ylabel('absorbance');
title('average spectra');
if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function chemmorphimages(self,ind,directory,suffix,paperpos)
if nargin < 2, ind = 1:6; end
if nargin < 3, dosave=0; else, dosave=1; end
if nargin < 4, suffix = '_fgroups'; end
if nargin < 5, paperpos = [0.25,2.5,5.5,4]; end
if isempty(ind),
  ind = 1:6;
end

[nx,ny] = rn2mfrow(length(ind));
figure('paperposition',paperpos)
count = 0;
for i=ind,
  count = count + 1;
    subplot(nx,ny,count)
    myimagesc(self,self.ccoeffs(:,:,i))    
    title(self.coeffnames(i))
    xlabel('x (\mum)'); ylabel('y (\mum)');        
end
if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');  
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function edgexyplots(self,mask,ind,directory,suffix)
if nargin < 2, mask = 'bw'; end
if nargin < 3, ind = 1:6; end
if nargin < 4, dosave=0; else, dosave=1; end

sh = shapemetrics;

%% edge matrix

d = feval(sh.distfun,self.xcoord,self.ycoord,self.(mask));

%% random sample
n = prod(size(d));
if n > 1500,
  smp = arrSubset(randperm(n),1:1500);
else
  smp = 1:n;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot
cond= {'marker','+','markeredgecolor',repmat(0.5,1,3)};
[nx,ny] = rn2mfrow(length(ind));
figure('paperposition',[0.25,2.5,5.5,4])
count = 0;
for i=ind,
  count = count + 1;
  subplot(nx,ny,count)    
  h = scatter(arrSubset(reshape(d,[],1),smp),...
	      arrSubset(reshape(self.ccoeffs(:,:,i),[],1),smp));
  set(h,cond{:});
  set(gca,'box','on','fontsize',6);
  title(self.coeffnames(i),'fontsize',7)
  xlabel('distance from surface','fontsize',6)
  ylabel('abundance','fontsize',6)
end
if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Kplots(self,directory,suffix)
if nargin < 2, dosave=0; else, dosave=1; end

if ~exist('RColors','var'),
  load RColors;
end

subx = inline('find(x >= b(1) & x <= b(2))','x','b');
i = subx(self.energy,self.kbounds);
plot(self.energy,self.kspec)
patch([self.energy(i),rrev(self.energy(i))],[self.kspec(i),repmat(0,1,length(i))],RColors.('steelblue'))
hline(0,'r-')
xlabel('energy (eV)'); ylabel('baselind residual');
title(strrep(self.name,'_','\_'));

if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function failedplots(self,directory,suffix)
if nargin < 2, dosave=0; else, dosave=1; end
close all;
subplot(1,2,1)
myimagesc(self,self.stackarr(:,:,getElem(abs(288.85-self.energy),@min,2)));
subplot(1,2,2)
myimagesc(self,self.totalc);
title(self.name);

if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');
end

return
