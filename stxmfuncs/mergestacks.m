%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~mergestacks.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = mergestacks(self,use)

if nargin < 2,
  use = [];
end

if self.nregions ==1,
  if length(self.energy) > size(self.stackarr,3),
    self.energy = self.energy(1:size(self.stackarr,3));
  end
elseif self.nregions > 1 & ~isempty(use),
  self.xcoord = self.xyregions{use}.xcoord;
  self.ycoord = self.xyregions{use}.ycoord;
  self.stackarr = self.multistacks{use};
  self = rmfield(self,'xyregions');
  self = rmfield(self,'multistacks');
  self.nregions = 1;
  if length(self.energy) > size(self.stackarr,3),  
    self.energy = self.energy(1:size(self.stackarr,3));
  end
elseif self.nregions > 1 & isempty(use),
  nxy = repmat(NaN,self.nregions,2);
  dxy = repmat(NaN,self.nregions,2);
  minxy = repmat(NaN,self.nregions,2);
  maxxy = repmat(NaN,self.nregions,2);
  for j = 1:self.nregions,
    nxy(j,:) = [length(self.xyregions{j}.xcoord),...
        length(self.xyregions{j}.ycoord)];
    dxy(j,:) = [median(diff(self.xyregions{j}.xcoord)),...
        median(diff(self.xyregions{j}.ycoord))];
    minxy(j,:) = [min(self.xyregions{j}.xcoord),...
        min(self.xyregions{j}.ycoord)];
    maxxy(j,:) = [max(self.xyregions{j}.xcoord),...
        max(self.xyregions{j}.ycoord)];
  end
  rg = [sum(maxxy(:,1)-minxy(:,1)),max(maxxy(:,2)-minxy(:,2))];
  res = min(dxy);
  
  %     gensp = inline('0:res(i):rg(i)','rg','res','i');
  %     pad1 = inline('[x,max(x)+res(i)]','x','res','i');    
  %     xypts = struct('xcoord',pad1(gensp(rg,res,1),res,1),...
  %         'ycoord',pad1(gensp(rg,res,2),res,2));
  npts = ceil(rg ./ res);
  xypts = struct('xcoord',linspace(0,rg(1),npts(1)),...
    'ycoord',linspace(0,rg(2),npts(2)));
  
  if length(self.energy) > size(self.multistacks{1},3),
    self.energy = self.energy(1:size(self.multistacks{1},3));
  end
  container = makebigarr(xypts,length(self.energy));
  nx = 1;
  for j = 1:self.nregions;
    iarr = interp2D(self.multistacks{j},self.xyregions{j},res);
    container = add2container(container,iarr,nx);
    nx = nx + size(iarr,2);        
  end
  self.xcoord = xypts.xcoord;
  self.ycoord = xypts.ycoord;
  self.stackarr = container;
  self = rmfield(self,'xyregions');
  self = rmfield(self,'multistacks');
end

return

function container = makebigarr(xypts,n);
container = repmat(NaN,[length(xypts.ycoord),length(xypts.xcoord),n]);
return

function iarr = interp2D(arr,xy,res)
f = inline('linspace(0,max(x)-min(x),ceil((max(x)-min(x))/res))','x','res');
[X,Y] = meshgrid(xy.xcoord,xy.ycoord);
[XI,YI] = meshgrid(f(xy.xcoord,res(2)),f(xy.ycoord,res(1)));
iarr = repmat(NaN,[size(XI),size(arr,3)]);
for i=1:size(arr,3),
  iarr(:,:,i) = interp2(X,Y,arr(:,:,i),XI,YI);
end

return

function container = add2container(container,iarr,nx)
container(1:size(iarr,1),nx:(nx+size(iarr,2)-1),:) = iarr;
return
