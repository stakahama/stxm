%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~imthreshk.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function selfORn = imthreshk(self,im,method,fieldnames,k,morphf,dotype,badPnts,op)
% self = object
% im = either energy, 'totalc', 'all', or other existing field in
% self
% method = 'Otsu' or 'kmeans'
% fieldnames = names of fields to be added to self, 
%   in order of 'bw', 'thres', and avespec
% k = integer (of label) or image or character ('largest')
% morphf is morphological function (character) to pass to bwmorph
% dotype = {'bw','count'}
% badPnts = indices of bad pixels

if isequal(im,'all'),
    localoption = 'doall';
    img = self.stackarr;
else
    localoption = 'nil';
    if isa(im,'char'),
      img = self.(im);
    else
      img = self.stackarr(:,:,getElem(abs(self.energy-im),@min,2));
    end
end
%% method
if nargin < 3 | isempty(method), 
    method = 'Otsu'; 
end
%% field names
if nargin < 4 | isempty(fieldnames),
    fieldnames = {'bw','thres','avespec'};
end
%% k is either an image with which to overlap, or 'largest' label
if nargin < 5 | isempty(k),
    k = 'largest';
end
%% morphological operator
if nargin < 6 | isempty(morphf),
  morphf = '';
end
%% 'count' or 'bw'
if nargin < 7 | isempty(dotype),
  dotype = 'bw';
end
%% badPnts
if nargin < 8 | isempty(badPnts),
    badPnts = [];
end
if nargin < 9 | isempty(op),
  op = 'union';
end


[BW,thres] = getbw(localoption,img,method,badPnts,morphf,op);

if strcmp(dotype,'count'),
  particles = bwlabel(BW,8);
  funcs = shapemetrics;
  diam = repmat(NaN,max(particles(:)),1);
  for i = 1:max(particles(:)),
    lab = particles;
    lab(lab~=i) = 0;
    diam(i) = feval(funcs.equivSphereRadius,self.xcoord,self.ycoord,...
      logical(lab)) * 2;
  end
  pixsize = median([diff(self.xcoord),diff(self.ycoord)]);
  % minval = round(prod(size(BW))*threshfac) * pixsize^2;
  circlearea = inline('pi.*diam.^2./4','diam');
  minval = circlearea(.12);
  parea = circlearea(diam);
  selectedk = find( parea > minval)';
  n = selectedk( rrev(getElem(parea(selectedk),@sort,2)) );
  selfORn = n;
elseif strcmp(dotype,'bw'),
  %% Label image
  if isa(k,'char'),
    newBW = getLargestLabel(BW);
  else
    if all(size(k) > 1), % k is an image
      [newBW,overlap] = overlappingLabel(BW,k);    
    else, % k is threshold index
      newBW = getLabelk(BW,k);
    end
  end
  
  %% Average Spectra
  if length(fieldnames) > 2,
    x = reshape(self.stackarr,size(self.stackarr,1)*size(self.stackarr,2),...
      size(self.stackarr,3));    
    x(~isfinite(x)) = NaN;
    aveSpec = nanmean(x(find(newBW),:));
  end
  
  %% Assignment
  for i=1:length(fieldnames),
    if i==1,
      self.(fieldnames{i}) = newBW;
    elseif i==2,
      if strcmp('thres',fieldnames{i}),
        val = thres;
      else,
        val = overlap;
      end
      self.(fieldnames{i}) = val;        
    elseif i==3,
      self.(fieldnames{i}) = aveSpec;
    else,
      'pass'
    end
  end
  selfORn = self;
end

return

%% ====================================================
function newBW = getLabelk(BW,k)

newBW = bwlabel(BW,8);
newBW(find(newBW ~= k)) = 0;
newBW = logical(newBW);

return

%% ====================================================
function [newBW,n] = overlappingLabel(BW,k)

alllabels = bwlabel(BW,8);
n = max(alllabels(:));
count = repmat(NaN,n,1);
for i=1:n,
    img = alllabels;
    img(img ~=i) = 0;
    overlapping = logical(img) + logical(k);
    count(i) = length(find(overlapping > 1));
end
newBW = alllabels;
if all(count==0),
  newBW = k;
  n = 0;
else,
  n = getElem(count,@max,2);
  newBW(newBW ~= n) = 0;
end
newBW = logical(newBW);

return
