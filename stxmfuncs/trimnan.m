%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~trimnan.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function varargout = trimnan(img,img2)

origdim = size(img);
kc = ~all(isnan(img));
kr = ~all(isnan(img'));
trimmedimg = img(kr,kc);
keepc = ~any(isnan(trimmedimg));
keepr = ~any(isnan(trimmedimg'));
trimmedimg = trimmedimg(keepr,keepc);
varargout = {trimmedimg,origdim};

if nargin > 1,
  t2 = img2(kr,kc);
  t2 = t2(keepr,keepc);
  varargout = [varargout,{t2}];
end

