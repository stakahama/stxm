%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~fitedges.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [fitpars,resnorm,exitflag] = fitedges(self,parms)
if nargin < 2,
  parms = [.5,.5,.5];
end

x = self.xcoord; y = self.ycoord;
coeffs = self.ccoeffs(:,:,7);
bw = self.bw;
r = (self.diameter/2);
lb = [0,0,0];
ub = [1,10,10];

D = distfun(x,y,bw);
x = D(:); y = coeffs(:);
valid = ~isnan(x) & ~isnan(y);
[fitpars,resnorm,resids,exitflag] = lsqcurvefit(@coreshellmodel,parms,x(valid),y(valid),lb,ub,[],r);

return

function D = distfun(x,y,bw)
pixunit = median(diff([x,y]));
D = bwdist(~bw)*pixunit;
return

function alpha = coreshellmodel(parms,D,r)
% parms = [rstar,ab,as]
rstar = parms(1);
ab = parms(2);
as = parms(3);
g = gamma(D,r,rstar);
alpha = g * ab + (1-g) * as;
return

function gammaval = gamma(D,r,rstar)
gammaval = zeros(size(D));
which = ( (D/r)/rstar <= 1 );
gammaval(which) = rstar * sin(acos(rstar^-1*(1-D(which)/r)))./sin(acos(1-D(which)/r));
return
