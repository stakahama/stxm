%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~mergearr.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function finalstack = mergearr(stack1,stack2)

if isempty(stack1),
  finalstack = stack2;
else,
  melt = inline('reshape(x,[],size(x,3))');
  molten1 = melt(stack1)';
  molten2 = melt(stack2)';  
  s1 = any(isnan(molten1));
  s2 = any(isnan(molten2));
  ind = find(s1 & ~s2);
  molten1(:,ind) = molten2(:,ind);
  finalstack = reshape(molten1',size(stack1));
end
