%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~extractshapes.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = extractshapes(self,im)

sh = shapemetrics;

if strcmp(im,'bw288'),
  refEV = getElem(abs(self.energy - 288.85),@min,2);  
else,
  refEV = getElem(abs(self.energy - im),@min,2);  
  im = [];
end

convexImage = feval(sh.convexity,self.(im));
[majoraxis,minoraxis] = feval(sh.elongation,self.(im));

edgePts = feval(sh.getEdges,self.(im));
convedgePts = feval(sh.getEdges,convexImage);
[maxr,minr] = feval(sh.inner_outer,self.xcoord,self.ycoord,...
		    self.stackarr(:,:,refEV), self.(im));
self.convexity = sum(edgePts(:)) / sum(convedgePts(:));      
self.elongation = minoraxis/majoraxis;
self.circularity = feval(sh.circularity,self.xcoord,self.ycoord, ...
			 self.(im));
self.shaperatio = minr/maxr;


return
