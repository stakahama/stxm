%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~readhdr.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = readhdr(filename)

% fid = fopen(filename,'r');
% hdr = fscanf(fid,'%s');
% fclose(fid);
% 
% fds = {'PAxis','QAxis','StackAxis'};
% self = struct('name',regexprep(filename,'\.hdr',''),'xcoord',[],'ycoord',[],'energy',[]);
% for i = 1:length(fds),
%     st = findstr(fds{i},hdr);
%     en = arrSubset(findstr(')',hdr(st:end)),1);
%     self.(arrSubset(fields(self),i+1)) = ...
%         eval(strcat('[',regexprep(hdr(st:(st+en-1)),...
%         '.*Points=\([0-9]+,(.+)\)','$1','tokenize'),']'));
% end
% 
% return

fid = fopen(filename,'r');
str = {};
for i = 1:1000,
    x = fgetl(fid);
    if isequal(x,-1),
        break
    else
        str = [str,{x}];         
    end
end
fclose(fid);

%% define useful functions
mchline = inline('str{arrSubset(find(~cellfun(''isempty'',regexp(str,x)))+p,i)}',...
    'str','x','i','p');
getpts = inline('regexprep(line,''.+Points = \(([0-9]+),(.*)\)'',''$2'',''tokenize'')',...
    'line');
pts2vec = inline('eval(sprintf(''[%s]'',x))','x');

%% number of regions
nregions = str2num(regexprep(mchline(str,'Regions',1,0),...
    '.+Regions = \(([0-9]),','$1','tokenize'));


%% initialize structure
fds = {'PAxis','QAxis'};
dynf = {'xcoord','ycoord'};
if nregions == 1,
	self = struct('name',regexprep(filename,'\.hdr',''),'energy',[],...
        'nregions',nregions,'xcoord',[],'ycoord',[]);
    for i = 1:length(fds);
        self.(dynf{i}) = pts2vec(getpts(mchline(str,fds{i},1,1)));
    end
else,
	self = struct('name',regexprep(filename,'\.hdr',''),'energy',[],...
        'nregions',nregions,'xyregions',{cell(nregions,1)});
	for j = 1:nregions,
        rxy = cell2struct(cell(1,2),dynf,2);
        for i = 1:length(fds);
            rxy.(dynf{i}) = pts2vec(getpts(mchline(str,fds{i},j,1)));
        end
        self.xyregions{j} = rxy;
	end
end
self.energy = pts2vec(getpts(mchline(str,'StackAxis',1,1)));

return
