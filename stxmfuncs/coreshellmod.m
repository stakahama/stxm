%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~coreshellmod.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function betatp = coreshellmod(r,Rp,Ri,betai,betao)

tp = 2*Rp*sin(acos(r/Rp));
ti = ifelse(r < Ri,2*Ri*sin(acos(r/Ri)),0);

if isnan(betai),
  betai = 0;
end

betatp = betao .* (tp-ti) + betai .* ti;
