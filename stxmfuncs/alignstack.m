%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~alignstack.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = alignstack(self,eV,frac)
if nargin < 3, frac = 0.75; end
refimage = self.stackarr(:,:,getElem(abs(self.energy-eV),@min,2));
for i=1:size(self.stackarr,3),
    self.stackarr(:,:,i) = alignImages(self.stackarr(:,:,i),refimage,frac);
end

return

function recovered_b = alignImages(b,A,f)
% 2-D alignment by cross-correlation
% b scaled to f and correlated to A (which is larger than size(b)*f)

if nargin < 3, f = 0.75; end

% create large container
newSizeb = floor(size(b)*f);
xybegin = floor(newSizeb*(1/f-1)/2);
xyend = xybegin+newSizeb-1;
smallb = b(xybegin(1):xyend(1),xybegin(2):xyend(2));
%
% A(find(isinf(A) | isnan(A))) = 0;
% newb(find(isinf(newb) | isnan(newb))) = 0;
c = normxcorr2(smallb,A);
% figure, surf(c), shading flat
[max_c,imax] = max(abs(c(:)));
[ypeak,xpeak]= ind2sub(size(c),imax(1));
offset = [(xpeak-size(smallb,2)),(ypeak-size(smallb,1))] - ...
    (xybegin-1);

xi = (1:size(b,2))+offset(1);
yi = (1:size(b,1))+offset(2);
xi = xi(find(xi >= 1 & xi <= size(b,2)));
yi = yi(find(yi >= 1 & yi <= size(b,1)));

if(offset(1) >= 1), x2i = 1:length(xi);
else, x2i = (size(b,2)-length(xi)+1):size(b,2); end
if(offset(2) >= 0), y2i = 1:length(yi);
else, y2i = (size(b,1)-length(yi)+1):size(b,1); end

recovered_b = repmat(NaN,size(A));
recovered_b(yi,xi) = b(y2i,x2i);

return
