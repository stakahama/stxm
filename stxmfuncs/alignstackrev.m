%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~alignstackrev.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = alignstackrev(self,eV,frac)
%% revision of stackalign
if nargin < 3, frac = 0.75; end
refimage = elf.stackarr(:,:,getElem(abs(self.energy-eV),@min,2));
for i=1:size(self.stackarr,3),
    self.stackarr(:,:,i) = alignImages(self.stackarr(:,:,i),refimage,frac);
end

return

function recovered = alignImages(img,refimg,f)
% 2-D alignment by cross-correlation
% img scaled by f and correlated to refimage (which is larger than size(b)*f)
% basic

if nargin < 3, f = 0.75; end
if isempty(f), f = 0.75; end

%% trim
trimsize = floor(size(img) * (1-f) /2)-1;
small = img((1+trimsize(1)):(end-trimsize(1)),...
    (1+trimsize(2)):(end-trimsize(2)));

%% cross correlation
c = normxcorr2(small,refimg);
% figure, surf(c), shading flat
[max_c,imax] = max(abs(c(:)));
[ypeak,xpeak]= ind2sub(size(c),imax(1));
offset = [(xpeak-size(small,2)),(ypeak-size(small,1))] - trimsize;

%% indices
xi = (1:size(img,2))+offset(1);
yi = (1:size(img,1))+offset(2);
xi = xi(find(xi >= 1 & xi <= size(b,2)));
yi = yi(find(yi >= 1 & yi <= size(b,1)));

if(offset(1) >= 0), x2i = 1:length(xi);
else, x2i = (size(img,2)-length(xi)+1):size(img,2); end
if(offset(2) >= 0), y2i = 1:length(yi);
else, y2i = (size(img,1)-length(yi)+1):size(img,1); end

%% recover image in refimg dimensions
recovered = repmat(NaN,size(refimg));
recovered(yi,xi) = img(y2i,x2i);

return
