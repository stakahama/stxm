%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~edgexyplots.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function edgexyplots(self,refev,mask,ind,directory,suffix)
if nargin < 3, mask = 'bw'; end
if nargin < 4, ind = 1:6; end
if nargin < 5, dosave=0; else, dosave=1; end

sh = shapemetrics;
global hsvm cminval

%% edge matrix
d = feval(sh.distfun,self.xcoord,self.ycoord,self.(mask));
%% radial matrix
theta = hsvangle(self,refev,mask);
bwtheta = theta; bwtheta(~self.(mask)) = cminval;

%% random sample
nmax = 1500; % maximum number of points to plot
n = prod(size(d));
if n > nmax,
  smp = arrSubset(randperm(n),1:nmax);
else
  smp = 1:n;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% plot
% cond= {'marker','+','markeredgecolor',repmat(0.5,1,3)};
cond= {'marker','+'};
[nx,ny] = rn2mfrow(length(ind)+1);
%%
figure('paperposition',[0.25,2.5,5.5,4])
colormap([1,1,1;hsvm]);
subplot(nx,ny,1)    
myimagesc(self,bwtheta)
set(gca,'clim',[cminval,2*pi])
title('angle and bw')
xlabel('x'); ylabel('y')
subplot(nx,ny,1)    
count = 1;
for i=ind,
  count = count + 1;
  subplot(nx,ny,count)    
  h = scatter(arrSubset(reshape(d,[],1),smp),...
	      arrSubset(reshape(self.ccoeffs(:,:,i),[],1),smp),6^2,...
        arrSubset(reshape(theta,[],1),smp));
  set(h,cond{:});
  set(gca,'clim',[cminval,2*pi]);
  set(gca,'box','on','fontsize',6);
  title(self.coeffnames(i),'fontsize',7)
  xlabel('distance from surface','fontsize',6)
  ylabel('abundance','fontsize',6)
end
if dosave,
  saveas(gcf,outfile(self,directory,suffix),'png');
end

return
%%%

function filename = outfile(self,directory,suffix)
filename = fullfile(directory,strcat(self.name,suffix));
return
