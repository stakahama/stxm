%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~overlaybwperim.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function overlaybwperim(x,y,im)

[X,Y] = meshgrid(x,y);
perim = bwperim(im);
h=line(arrSubset(X(:),find(perim(:))),...
  arrSubset(Y(:),find(perim(:))))
set(h,'lines','none','marker','+');

return
