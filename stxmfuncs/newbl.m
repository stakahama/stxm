%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~newbl.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [out,cf] = newbl(ev,spec,meth)

if nargin < 3, meth=1; end

ind = find(ev > 278 & ev < 283);
if meth == 1, %% y~x
  cf = regress(reshape(spec(ind),[],1),[repmat(1,length(ev(ind)),1),reshape(ev(ind),[],1)]);
  out = cf(1) + cf(2) * ev;
elseif meth == 2, %% y~1/x
  cf = regress(reshape(spec(ind),[],1),[repmat(1,length(ev(ind)),1),reshape(ev(ind).^-1,[],1)]);
  out = cf(1) + cf(2) * ev.^-1;
elseif meth==3, %% y~x
  ind = [ind;length(ev)];  
  cf = regress(reshape(spec(ind),[],1),[repmat(1,length(ev(ind)),1),reshape(ev(ind),[],1)]);
  out = cf(1) + cf(2) * ev;
elseif meth==4, %% y~x + x^2
  ind = [ind;length(ev)];  
  cf = regress(reshape(spec(ind),[],1),[repmat(1,length(ev(ind)),1),reshape(ev(ind),[],1),...
      reshape(ev(ind).^2,[],1)]);
  out = cf(1) + cf(2) * ev  + cf(3) * ev.^2;
elseif meth==5, %% y~1/x
  ind = [ind;length(ev)];  
  cf = regress(reshape(spec(ind),[],1),[repmat(1,length(ev(ind)),1),reshape(ev(ind).^-1,[],1)]);
  out = cf(1) + cf(2) * ev.^-1;
elseif meth==6, %% robust fit
  ind = [ind;length(ev)];  
  W = diag(ifelse(ev(ind) < 300,1,20));
  y = reshape(spec(ind),[],1);
  X = [repmat(1,length(ev(ind)),1),reshape(ev(ind).^-1,[],1)];
  cf = inv(X' * W * X) * X' * W * y;
  out = cf(1) + cf(2) * ev.^-1;
end

return
