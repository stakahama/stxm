function self = stackregister(self,refen)

[optimizer, metric] = imregconfig('multimodal');
% $$$ optimizer.MaximumIterations = 300;

[minval,refidx] = min(abs(self.energy-refen));
fixed = self.stackarr(:,:,refidx);

for k=1:size(self.stackarr,3),
    if k==refidx,
        continue
    end
    self.stackarr(:,:,k) = imregister(self.stackarr(:,:,k),fixed,'translation',optimizer,metric);
end

