%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~shapemetrics.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function F = shapemetrics()
%% some functions need checking,
%% especially with x-y transposition
%% coordmats is the same as meshgrid
%% distfun is (almost) the same as bwdist

F = struct('getRadius',@getRadius,...
    'findcenter',@findcenter,'calcR',@calcR,...
    'replaceBad',@replaceBad,'distfun',@distfun,...
    'coordmats',@coordmats,...
    'fullRadius',@fullRadius,'getMaxR',@getMaxR,...
    'equivSphereRadius',@equivSphereRadius,'convexity',@convexity,...
    'elongation',@elongation,'plotImageMetrics',@plotImageMetrics,...
    'getEdges',@getEdges,'inner_outer',@inner_outer,...
    'elongationPlotparams',@elongationPlotparams,...
    'circularity',@circularity);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [radius,xybar,theta,allxy] = getRadius(x,y,totalarr,maskarr)

xybar = findcenter(x,y,totalarr,maskarr);
[xmat,ymat] = coordmats(x,y);
allxy = struct('x',xmat,'y',ymat);

radius = sqrt((xmat(:)-xybar{1}).^2 + (ymat(:)-xybar{2}).^2);
radius = reshape(radius,size(xmat));

DX = xmat(:)-xybar{1};
DY = ymat(:)-xybar{2};
theta = atan(abs(DY)./abs(DX)).*180/pi;
theta = (DX >= 0 & DY >= 0) .* theta + ...
    (DX < 0 & DY >= 0) .* (180-theta) + ...
    (DX < 0 & DY < 0) .* (180+theta) +...
    (DX >= 0 & DY < 0) .* (360-theta);
theta = reshape(theta,size(xmat));
return

%% subfunction
function xybar = findcenter(x,y,totalarr,maskarr)

totalarr(~logical(maskarr)) = NaN;
totalarr(~isfinite(totalarr)) = NaN;

[xmat,ymat] = coordmats(x,y);

xbar = nansum(xmat(:).*totalarr(:))/nansum(totalarr(:));
ybar = nansum(ymat(:).*totalarr(:))/nansum(totalarr(:));

xybar = {xbar,ybar};
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xmat,ymat] = coordmats(x,y)

xmat = repmat(reshape(x,1,[]),length(y),1);
ymat = repmat(reshape(y,[],1),1,length(x));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [maxR, edgeDist] = calcR(radius,theta,n,rm)
%% radius
%% theta
%% n = number of interpolation points
%% rm = remove these points

if nargin ==4,
    radius(rm) = NaN;
    theta(rm) = NaN;
end

if nargin < 3, n=31; end
nTheta = linspace(0,360,n);
maxR = repmat(NaN,length(nTheta)-1,1);
edgeDist = repmat(NaN,size(theta));
for m=2:length(nTheta),
    sub = find(theta >= nTheta(m-1) & theta < nTheta(m));
    subr = radius(sub);
    if isempty(subr) | all(isnan(subr)), continue, end
    R = nanmax(subr);
    edgeDist(sub) = R - subr;            
    maxR(m-1) = R;        
end
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function arr = replaceBad(arr,badPts)
% function arr = replaceBad(arr,badPts)

arr(badPts) = NaN;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function edgemat = distfun(x,y,bw)

[xmat,ymat] = coordmats(x,y);
edgemat = double(bw);
for pix = find(bw)',
    edgemat(pix) = min(reshape(sqrt((xmat(~bw) - xmat(pix)).^2 + ...
        (ymat(~bw) - ymat(pix)).^2),[],1));
end
edgemat(~bw) = NaN;

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [aveR,r,edgePts] = fullRadius(x,y,totalarr,maskarr),
% function [aveR,r] = fullRadius(S),

[xmat,ymat] = coordmats(x,y);
edgemat = distfun(x,y,maskarr);
xybar = findcenter(x,y,totalarr,maskarr);
edgePts = getEdges(maskarr);
r = sqrt((xmat(edgePts) - xybar{1}).^2 + (ymat(edgePts) - xybar{2}).^2);
aveR = mean(r); % only good for convex shapes

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [a,b] = getMaxR(x,y,totalarr,maskarr,n)

if nargin < 3, n=31; end
[xmat,ymat] = coordmats(x,y);
edgemat = distfun(x,y,maskarr);
xybar = findcenter(x,y,totalarr,maskarr);
edgePts = getElem({x,y,totalarr,maskarr},@fullRadius,3);
DX = xmat(edgePts) - xybar{1};
DY = ymat(edgePts) - xybar{2};
theta = atan(abs(DY)./abs(DX)).*180/pi;
theta = (DX >= 0 & DY >= 0) .* theta + ...
    (DX < 0 & DY >= 0) .* (180-theta) + ...
    (DX < 0 & DY < 0) .* (180+theta) + ...
    (DX >= 0 & DY < 0) .* (360-theta);
thetdiv = linspace(0,360,n);
a = repmat(NaN,n-1,1);
b = repmat(NaN,n-1,1);
for i=2:length(thetdiv),
        j = find(theta(:) >= thetdiv(i-1) & theta(:) < thetdiv(i));
        [a(i-1),c] = max(sqrt(DX(j).^2+DY(j).^2));
        b(i-1) = theta(j(c));
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function radius = equivSphereRadius(x,y,bw),

dx = median(diff(x));
dy = median(diff(y));
shapearea = dx*dy*length(find(bw(:)));
radius = sqrt(shapearea/pi);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [img,huLL] = convexity(arr)
% convexity
% function [img,huLL] = conveXity(arr)
% arr is binary image

cvh = regionprops(double(arr),{'ConvexImage','ConvexHull','BoundingBox'});

flrceil = {@floor,@ceil};
oldmetric = 1e3; metric = 1e3;
for i = 1:2,
    if metric < 1,
        break
    end    
    for j = 1:2,
        xrg = feval(flrceil{i},cvh.BoundingBox(2)) + (1:cvh.BoundingBox(4));
        yrg = feval(flrceil{j},cvh.BoundingBox(1)) + (1:cvh.BoundingBox(3));
        tmp = zeros(size(arr));
        tmp(xrg,yrg) = cvh.ConvexImage;        
        metric = sum( (tmp(:)-arr(:) < 0) );
        if metric < 1,
            img = tmp;
            break       
        else
            if( metric < oldmetric ),
                img = tmp;
                oldmetric = metric;
            end
        end
    end
end

huLL = cvh.ConvexHull;
%tmpf

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [a,b,th,z] = elongation(arr)
% elongation
% function [a,b,th,z] = eLongation(arr,d)
% d = median(diff([x;y]));
% arr is binary image

th = cell2mat(struct2cell(regionprops(double(arr'),'Orientation')))/180*pi;
c = struct2cell(regionprops(double(arr'),...
    {'MajorAxisLength','MinorAxisLength'}));
[a,b] = deal(c{:}); clear c;

return

function z = elongationPlotparams(a,b,th,d,scale)
	B = [cos(th),-sin(th);sin(th),cos(th)];
	thet = linspace(-pi,pi,100);
	x = (a .* cos(thet)) .* d;
	y = (b .* sin(thet)) .* d;
	z = (inv(B) * [x/max(x)*scale;y/max(x)*scale])';

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotImageMetrics(S,arrnm,convIm,convHull,edgePts,xybar,z,maxr,minr)
% function plotImageMetrics(S,arr,convIm,convHull,edgePts,xybar,z)

xmin = getElem(abs(S.XValues-xybar{1}),@min,2);
ymin = getElem(abs(S.YValues-xybar{2}),@min,2);
d = median([diff(S.XValues);diff(S.YValues)]);
arr = S.(arrnm);

thet = linspace(-pi,pi,100);

colormap(bone)
subplot(1,2,1)
tmp = zeros(size(arr));
tmp(find(convIm - arr > 0)) = 1;
tmp(find(arr > 0)) = 2;
myimagesc(S,tmp)
set(gca,'xlimmode','auto','ylimmode','auto')
line(xybar{1}+z(:,1),xybar{2}+z(:,2),'color','r','linew',1.5)
line((convHull(:,2) - xmin).*d+xybar{1},(convHull(:,1) - ymin).*d + xybar{2},...
    'color','y','linew',1.5)
xrg = get(gca,'xlim');
yrg = get(gca,'ylim');
subplot(1,2,2)
myimagesc(S,arr + edgePts)
x = maxr .* cos(thet);
y = maxr .* sin(thet);
line(x+xybar{1},y+xybar{2},'color','r','linew',1.5);
x = minr .* cos(thet);
y = minr .* sin(thet);
line(x+xybar{1},y+xybar{2},'color','r','linew',1.5);
set(gca,'xlim',xrg,'ylim',yrg);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function edgePts = getEdges(bw),
% function edgePts = getEdges(S,M)

labeledIm = bwlabel(bw,8);
largestLab = getLargestLabel(labeledIm);
edgePts = bwperim(largestLab);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [maxr,minr] = inner_outer(varargin)

r = getElem(varargin,@fullRadius,2);
maxr = nanmax(r(:));
minr = nanmin(r(:));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function metric = circularity(x,y,bw)
%% circularity or compactness

edgePts = getEdges(bw);
metric = sum(edgePts(:).^2) / (4 * pi * sum(bw(:)));

return
