%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~loadCfunctions.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function F = loadCfunctions()

F = struct('Cavefit',@Cavefit,'Cbypix',@Cbypix,...
	   'selectckernel',@selectckernel,...
	   'normalizeC',@normalizeC,...
	   'Cpeakfits',@Cpeakfits,...
	   'nameccoeffs',@nameccoeffs,...
	   'getArgs',@getArgs,...
	   'integrpotassium',@integrpotassium);%,'Cpeakfits',@Cpeakfits);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% higher level functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fit average spectra
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function self = Cavefit(self,method)
if nargin < 2, method = 1; end
[A,bgC,totC] = normalizeC([[self.energy]',[self.avespec]']);
coeffs = Cpeakfits(A(:,1),A(:,2),method);    
self.aveccoeffs = [bgC,totC,coeffs'];
self = addcnames(self,method);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% integrate residual for potassium
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function self = integrpotassium(self,method)
[self.kval, self.kspec, self.kbounds] = getK(self.energy,self.avespec,self.aveccoeffs(3:end),method);
return 

function n = ncoeffs(method)
n = length(nameccoeffs(method));
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fit spectra by pixel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function self = Cbypix(self,maskstring,method,badEnergies)
if nargin < 2, maskstring = 'bw'; end
if nargin < 3, method = 1; end
if nargin < 4, badEnergies = []; end
mask = self.(maskstring)';
n = ncoeffs(method);
%% empty arrays
Ccoeffs = repmat(NaN,[size(self.stackarr,1),size(self.stackarr,2), n + 1]);
% bgtotC = repmat(NaN,[size(self.stackarr,1),size(self.stackarr,2), 2]);
%% do fitting
[A,B] = meshgrid(1:size(self.stackarr,1),1:size(self.stackarr,2));
ij = [A(:),B(:)];
ij = ij(mask(:),:);
clear A B masked
rmax = size(ij,1);
for r=1:rmax,
  disp([r,rmax,ij(r,:)]);         
  try,
    [Cspec,bgC,totC] = normalizeC([self.energy;...
        reshape(self.stackarr(ij(r,1),ij(r,2),:),1,[])]');
    whatfailed = 'peakfits';
    p = Cpeakfits(Cspec(:,1),Cspec(:,2),method)';
    Ccoeffs(ij(r,1),ij(r,2),1:n) = [bgC,totC,p]';
    whatfailed = 'Kfit';
    Ccoeffs(ij(r,1),ij(r,2),end) = getK(Cspec(:,1)',Cspec(:,2)',p,method);
  catch,
    disp(sprintf('failed: %s',whatfailed));
  end
end       
% self.ccoeffs = reshape([bgtotC(:);Ccoeffs(:)],...
% 		       [size(Ccoeffs,1),size(Ccoeffs,2),...
% 		    size(bgtotC,3) + size(Ccoeffs,3)]);
self.ccoeffs = Ccoeffs;
self = addcnames(self,method,{'K'});
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add names to object (structure)
%% called by fitting functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function self = addcnames(self,method,morenames)
if nargin < 2, method = 1; end
if nargin < 3,
  self.coeffnames = nameccoeffs(method);
else,
  self.coeffnames = [nameccoeffs(method),morenames];
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% method-specific functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% get initial values and bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x0,lb,ub] = getinitbounds(method)
if method==1,
	lb = repmat(0,11,1); lb(9:10) = 1;
	ub = [repmat(1e3,1,6),3,3,5,8,1e3]';
	x0 = repmat(0.1,11,1); x0(9:10) = 1.01;   
elseif method==2,
	lb = [repmat(0,1,9),repmat(0.5,1,8)]';
	ub = [repmat(10,1,9),repmat(2,1,6),5,8]';
	x0 = [repmat(0.5,1,8),1,repmat(1,1,8)]';    
elseif method ==3,
	lb = repmat(0,11,1); lb(10:11) = 1;
	ub = [repmat(1e3,1,7),3,3,5,8]';
	x0 = repmat(0.1,11,1); x0(10:11) = 1.01;
elseif method==4,
	lb = repmat(0,12,1); lb(10:11) = 1;
	ub = [repmat(1e1,1,7),3,3,5,8,1e1]';
	x0 = repmat(0.1,12,1); x0(10:11) = 1.01;
elseif method==5,
	lb = repmat(0,16,1); lb(12:13) = 1; lb(14:15) = 0.1;
	ub = [repmat(1e1,1,7),3,3,1e1,1e1,5,8,3,3,1e1]';
	x0 = repmat(0.1,16,1); x0(12:13) = 1.01; x0(14:15) = 0.11;
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% coefficient names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function coeffnames = nameccoeffs(method)
if method == 1,
	coeffnames = {'bg','totc','C=C','C=O','C-H','COOH',...
        'CNH-COH','CO3','sigma1','sigma2','sigma1w','sigma2w','arctan'};
elseif method == 2,
	coeffnames = {'bg','totc','C=C','C=O','C-H','COOH',...
        'CNH-COH','CO3','sigma1','sigma2','arctan',...
        'C=Cw','C=Ow','C-Hw','COOHw','CNH-COHw','CO3w',...
        'sigma1w','sigma2w'};
elseif method == 3,
	coeffnames = {'bg','totc','bzqC=C','C=C','C=O','C-H','COOH',...
        'CNH-COH','CO3','sigma1','sigma2','sigma1w','sigma2w'};
elseif method == 4,
	coeffnames = {'bg','totc','bzqC=C','C=C','C=O','C-H','COOH',...
        'CNH-COH','CO3','sigma1','sigma2','sigma1w','sigma2w','arctan'};
elseif method == 5,
	coeffnames = {'bg','totc','bzqC=C','C=C','C=O','C-H','COOH',...
        'CNH-COH','CO3','sigma1','sigma2','K1','K2',...
        'sigma1w','sigma2w','K1w','K2w','arctan'};
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% extra arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xa,ya,extraArgs] = getArgs(x,y,method)
if method==5,
  %% Kpeakloc
  i = find(x > 295. & x < 298.5);
  j = find(x > 298.5 & x < 302);  
  extraArgs = [arrSubset(x(i),getElem(y(i),@max,2)),...
      arrSubset(x(j),getElem(y(j),@max,2))];
  xa = x; ya = y;
else
  extraArgs = [];  
  i = find(x < 295 | x > 302);
  xa = x(i); ya = y(i);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% lower level functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% do non-linear least-squares fitting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fitpars = Cpeakfits(xdata,ydata,method)
[x0,lb,ub] = getinitbounds(method);
[newx, newy] = interpxy(xdata,ydata);
[newx, newy, extraArgs] = getArgs(newx,newy,method);
% global extraArgs
fitpars = lsqcurvefit(@cfit,x0,newx,newy,lb,ub,[],method,extraArgs);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Summing function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pksums = cfit(p,x,meth,extraArgs)
if nargin < 4, extraArgs = []; end
if length(extraArgs) > 0,
  pksums = sum(feval(selectckernel(meth),p,x,extraArgs))';
else
  pksums = sum(feval(selectckernel(meth),p,x))';
end  
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% interpolate x and y values (default n=250)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [newx, newy] = interpxy(xdata,ydata,n)
if nargin < 3, n = 250; end
newx = linspace(xdata(1),xdata(end),n)';
[xdata,ydata] = stripnan(xdata,ydata);
newy = interp1(xdata,ydata,newx);
return

function [xp,yp] = stripnan(x,y)
if any(isnan(y)),
  xp = x(~isnan(y));
  yp = y(~isnan(y));
else,
  xp = x;
  yp = y;
end
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% scale,normalize by pre-edge and post-edge
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [output,bgC,totC] = normalizeC(A,maxev,meth);
if nargin < 2, maxev = 320; end
if nargin < 3, meth = 0; end
x = A(:,1);
if meth==0,
  try, bgC = integrave(A,[278,283]);
  catch, bgC = nanmean(A(find(A(:,1) > 278 & A(:,1) < 283),2));
  end    
  sansbg = A(:,2)-bgC;
else,
  [y,cf] = newbl(x,A(:,2),meth);
  bgC = cf(1);
  sansbg = A(:,2)-y;
end
tol = 1;
if maxev <= 310,
	try, totC = integrave([x,sansbg],[301,305]);
	catch, totC = nanmean(sansbg(find(x > 301 & x < 305+tol)));
    end
elseif maxev > 310 & maxev <= 325,
	try, totC = integrave([x,sansbg],[305,320]);
	catch, totC = nanmean(sansbg(find(x > 305 & x < 320+tol)));
    end
end
scaled = sansbg/totC;
output = [x,scaled];
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% integrate residual for potassium
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [val,kspec,bounds] = getK(energy,avespec,coeffs,method)
cfun = loadCfunctions;
subx = inline('find(x >= b(1) & x <= b(2))','x','b');

%% residual
resid = arrSubset(feval(cfun.normalizeC,[energy',avespec']),[],2)' - ...
  sum(feval(feval(cfun.selectckernel,method),coeffs',energy'));

%% baseline
fitted = inline('p(1)*x + p(2)','p','x');
regn = {[295+[-1,1];298.35+.5*[-1,1]],[298.35+.5*[-1,1];302.5+[-1,1]]};
blmat = repmat(NaN,length(energy),2);
for i=1:length(regn),
  j = sort(unique([subx(energy,regn{i}(1,:)),subx(energy,regn{i}(2,:))]));
  p = polyfit(energy(j),resid(j),1);
  blmat(:,i) = fitted(p,energy)';
end
intersection = energy(getElem(abs(diff(blmat')),@min,2));
bl = [blmat(energy < intersection,1);...
    blmat(energy >= intersection,2)]';
kspec = resid - bl;

% i = sort([subx(energy,[293,295]),subx(energy,[302,305])]);
% p = polyfit(energy(i),resid(i),1);
% fitted = inline('p(1)*x + p(2)','p','x');
% kspec = resid - fitted(p,energy);

%% integration bounds
% i = subx(energy,[293,297]);
% j = subx(energy,[299,305]);
% bounds = [arrSubset(energy(i),getElem(avespec(i),@min,2)),...
%     arrSubset(energy(j),getElem(avespec(j),@min,2))];

bounds = [];
for c = {[294,296],[300,304]},
  i = subx(energy,c{:});
  bounds = [bounds,arrSubset(energy(i),getElem(abs(kspec(i)),@min,2))];
end

%% integrate
i = subx(energy,bounds);
integf = inline('nansum(0.5*(y(2:end)+y(1:(end-1))).*diff(x))','x','y');
val = integf(energy(i), arrSubset(kspec,i));

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% select kernel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fitfunction = selectckernel(method)
fitfunction = eval(sprintf('@CarbonfitKernel%d',method));
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Fitting kernels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fixed peaks - original
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fit = CarbonfitKernel(M,xdata)
peakloc = [285,286.7,287.7,288.7,289.5,290.4,294,303];
% 1-6, pi* height
% 7-8, sigma* height
% 9-10, sigma* half-width
% 11, arctanstep
y = zeros(9,length(xdata));
for i=1:11
  if i <= 6, % pi*
    y(i,:) = [M(i)*gaussian(xdata,peakloc(i),1)]';
  elseif i > 6 & i <= 8, % sigma*
    y(i,:) = [M(i)*gaussian(xdata,peakloc(i),M(i+2))]';        
  elseif i == 11  % arctanstep
    y(9,:) = [M(i)*arctanstep(xdata,290,1) - ...
        min(M(i)*arctanstep(xdata,290,1))]';
  end      
end
fit = y;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% variable-width peaks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fit = CarbonfitKernel2(par,xdata)
peakloc = [285,286.7,287.7,288.7,289.5,290.4,294,303];
% 1-6, pi* height
% 7-8, sigma* height
% 9, arctanstep
% 10-17, sigma* half-width
y = zeros(9,length(xdata));
for i=1:9,
  if i <= 8, % sigma*
    y(i,:) = [par(i)*gaussian(xdata,peakloc(i),par(i+9))]';        
  elseif i == 9  % arctanstep
    y(i,:) = [par(i)*arctanstep(xdata,290,1) - min(par(i)*arctanstep(xdata,290,1))]';
  end      
end
fit = y;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fixed peaks - fixed arctan + benzoquinone at 284
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fit = CarbonfitKernel3(par,xdata)
peakloc = [284,285,286.7,287.7,288.7,289.5,290.4,294,303];
% 1-7, pi* height
% 8-9, sigma* height
% 10-11, sigma* half-width
% fixed -> arctanstep
fit = zeros(10,length(xdata));
for i=1:11
  if i <= 7, % pi*
    fit(i,:) = [par(i)*gaussian(xdata,peakloc(i),1)]';
  elseif i > 7 & i <= 9, % sigma*
    fit(i,:) = [par(i)*gaussian(xdata,peakloc(i),par(i+2))]';        
  end      
end
scale = inline('(x-min(x)) ./ (max(x)-min(x))','x');
fit(10,:) = scale(arctanstep(xdata,290,1))'; % carbonedge
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fixed peaks - meth1 + benzoquinone at 284
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fit = CarbonfitKernel4(par,xdata)
peakloc = [284,285,286.7,287.7,288.7,289.5,290.4,294,303];
% 1-7, pi* height
% 8-9, sigma* height
% 10-11, sigma* half-width
% 12, arctan
scale = inline('p*(x - min(x))','p','x');
fit = zeros(10,length(xdata));
for i=1:12
  if i <= 7, % pi*
    fit(i,:) = [par(i)*gaussian(xdata,peakloc(i),1)]';
  elseif i > 7 & i <= 9, % sigma*
    fit(i,:) = [par(i)*gaussian(xdata,peakloc(i),par(i+2))]';        
  elseif i==12
    fit(10,:) = scale(par(i),arctanstep(xdata,290,1))';
  end      
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fixed peaks - same as 4 but with K+
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fit = CarbonfitKernel5(par,xdata,Kpeakloc)
if nargin < 3, Kpeakloc = [297.4,299.9]; end
peakloc = [284,285,286.7,287.7,288.7,289.5,290.4,294,303,Kpeakloc];
% 1-7, pi* height
% 8-9, sigma* height
% 10-11, sigma* half-width
% 12, arctan
scale = inline('p*(x - min(x))','p','x');
fit = zeros(12,length(xdata));
for i=1:16
  if i <= 7, % pi*
    fit(i,:) = [par(i)*gaussian(xdata,peakloc(i),1)]';
  elseif i > 7 & i <= 11, % sigma*
    fit(i,:) = [par(i)*gaussian(xdata,peakloc(i),par(i+2))]';        
  elseif i==16,
    fit(12,:) = scale(par(i),arctanstep(xdata,290,1))';
  end      
end
return

