%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getedges.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [x,y,bw,r] = getedges(self,n)

if nargin < 2,
  n = 7;
end

x = self.xcoord; y = self.ycoord; bw = self.bw;
D = distfun(x,y,bw);
coeffs = self.ccoeffs(:,:,n);

r = (self.diameter/2);
x = D(:); y = reshape(coeffs(:),[],length(n)); bw = bw(:);

return

function D = distfun(x,y,bw)
pixunit = median(diff([x,y]));
D = bwdist(~bw)*pixunit;
return
