%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~despeckle.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = despeckle(self)

if self.nregions == 1,
    self.stackarr = despecklesinglestack(self.stackarr);
else,
    for j = 1:self.nregions,
        self.multistacks{j} = despecklesinglestack(self.multistacks{j});
    end
end

return

function arr = despecklesinglestack(arr)

for i = 1:size(arr,3),
    bw = isfinite(arr(:,:,i));
    arr(:,:,i) = medfilt2(arr(:,:,i));
    arr(~logical(sum(bw,2)),:,i) = NaN;
    arr(:,~logical(sum(bw,1)),i) = NaN;    
end

return
