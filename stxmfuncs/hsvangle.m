%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~hsvangle.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function theta = hsvangle(self,refev,mask)

radialobj = radialdistfun(self,refev,self.(mask));
[outp{1:4},xy] = deal(radialobj{:});

%% USE COLORMAP(HSV)
[XY{1:2}] = meshgrid(self.xcoord,self.ycoord);
Rmat = sqrt((XY{1}-xy{1}).^2 + (XY{2}-xy{2}).^2);
cost = acos((XY{1}-xy{1})./Rmat);
sint = asin((XY{2}-xy{2})./Rmat);
cost(sint < 0) = 2*pi-cost(sint < 0);

theta = cost;
return
