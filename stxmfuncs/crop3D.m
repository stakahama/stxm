%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~crop3D.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function arr = crop3D(arr)
%% arr is 3-D array

resh1 = reshape(arr,[],size(arr,3));
resh2= ~reshape(sum(isnan(resh1),2),arrSubset(size(arr),1:2));
arr = arr(logical(sum(resh2,2)),logical(sum(resh2,1)),:);

return
