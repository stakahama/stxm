%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~countparticles-notused.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function n = countparticles(self,im,morphf,method,badPnts)
% self = object
% im = either energy, 'totalc', 'all', or other existing field in self
% fieldnames = names of fields to be added to self, 
%   in order of 'bw', 'thres', and avespec
% threshfac = threshold factor for particle id. suggested 0.01-0.1
% method = 'Otsu' or 'kmeans'
% badPnts = indices of bad pixels

if isequal(im,'all'),
    localoption = 'doall';
    img = self.stackarr;
else
    localoption = 'nil';
	if isa(im,'char'),
        img = self.(im);
	else
        img = self.stackarr(:,:,getElem(abs(self.energy-im),@min,2));
    end
end
if nargin < 4 | isempty(morphf),
  morphf = '';
end
%% method
if nargin < 5 | isempty(method), 
    method = 'Otsu'; 
end
%% badPnts
if nargin < 6 | isempty(badPnts),
    badPnts = [];
end

[BW,thres] = getbw(localoption,img,method,badPnts,morphf);

particles = bwlabel(BW,8);
funcs = shapemetrics;
diam = repmat(NaN,max(particles(:)),1);
for i = 1:max(particles(:)),
    lab = particles;
    lab(lab~=i) = 0;
    diam(i) = feval(funcs.equivSphereRadius,self.xcoord,self.ycoord,...
        logical(lab)) * 2;
end
pixsize = median([diff(self.xcoord),diff(self.ycoord)]);
% minval = round(prod(size(BW))*threshfac) * pixsize^2;
circlearea = inline('pi.*diam.^2./4','diam');
minval = circlearea(.12);
parea = circlearea(diam);
selectedk = find( parea > minval)';
n = selectedk( rrev(getElem(parea(selectedk),@sort,2)) );

%% I just added this npixels thing 02/18/09
%% may have tried it before but just forgotten.

return
