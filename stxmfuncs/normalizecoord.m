%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~normalizecoord.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = normalizecoord(self)

if self.nregions == 1,
    self = normalize1(self);
else,
	for j = 1:self.nregions,
        self.xyregions{j} = normalize1(self.xyregions{j});
	end
end
return

function obj = normalize1(obj),
for i = {'xcoord','ycoord'},
    obj.(char(i)) = obj.(char(i)) - min(obj.(char(i)));
end
return
