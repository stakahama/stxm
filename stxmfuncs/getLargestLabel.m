%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getLargestLabel.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function newBW = getLargestLabel(BW)
% function newBW = getLargestLabel(BW)

newBW = bwlabel(BW,8);
imlabels = unique(newBW(newBW~=0))';
a = repmat(NaN,1,max(imlabels));
for i=imlabels,
    a(i) = length(find(newBW==i));
end
newBW(find(newBW ~= getElem(a,@max,2))) = 0;
newBW = logical(newBW);

return
