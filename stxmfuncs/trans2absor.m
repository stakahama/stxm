%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~trans2absor.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = trans2absor(self,domorph,refev)
if nargin < 2, domorph = 0; end
if nargin < 3, fullbw = 1; else fullbw = 0; end

stackarr = self.stackarr;
%% threshold
BW = repmat(0,arrSubset(size(stackarr),1:2));
if fullbw,
  for i = 1:size(stackarr,3),
    %% add medfilt2 optional
    grayimage = mat2gray(stackarr(:,:,i));
    thres = graythresh(grayimage);
    bwim = ~im2bw(grayimage,thres);    
    if domorph,
      %% (1) convert to bw, (2) open, (3) fill
      bwim = bwmorph(bwmorph(bwim,'open'),'fill');
    end
    BW = BW + bwim;
  end
else,
  grayimage = mat2gray(stackarr(:,:,getElem(abs(self.energy-refev),@min,2)));
  thres = graythresh(grayimage);
  BW = ~im2bw(grayimage,thres);    
end

if all(logical(BW(:))) | sum(~logical(BW(:)))/prod(size(BW)) < 0.05,
  %% force background region
  alpha = 0.5;
  clusts = kmeans(BW(:),2);
  bwthres = alpha * min([mean(BW(clusts==1)),mean(BW(clusts==2))]) + ...
    (1-alpha) * max([min(BW(clusts==1)),min(BW(clusts==2))]);
  BW(BW < bwthres) = 0;
end

%% calculate I0
stck = reshape(stackarr,prod(arrSubset(size(stackarr),1:2)),size(stackarr,3));
I0 = permute(repmat(nanmean(stck(reshape(~logical(BW),[],1),:)),...
    [size(stackarr,2),1,size(stackarr,1)]),[3,1,2]);

self.stackarr = -log10(self.stackarr ./ I0);
self.stackarr(~isfinite(self.stackarr)) = NaN;

return
