%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getaspects.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function asp = getaspects(self,bw,largest,dotype);
if ischar(bw), bw = self.(bw); end
if nargin < 3, largest=0; end
if nargin < 4, dotype = ''; end
if strcmp(dotype,'')==1,
  asp = rrev(aspectpars(self.xcoord,self.ycoord,bw,largest));
elseif strcmp(dotype,'ellipse')==1,
  asp = rrev(aspectparse(self.xcoord,self.ycoord,bw,largest));
end
return

function aspects = aspectpars(x,y,bw,lg)

rp = regionprops(bwlabel(bw),'orientation');
theta = rp.Orientation;
if abs(theta) < 45
  theta = -theta;
else
  theta = 90 - theta;
end
bwr = imrotate(bw,theta);

try,
  bwrm = bwmorph(bwmorph(bwr,'close'),'open');  
  rp = regionprops(bwlabel(bwrm),'boundingbox');
  ce = num2cell(rp.BoundingBox(3:4));
catch,
  if lg,
    rp = regionprops(bwlabel(getLargestLabel(bwr)),'boundingbox');    
  else,
    rp = regionprops(bwlabel(bwr),'boundingbox');
  end
  ce = num2cell(rp.BoundingBox(3:4));
end  
[nx, ny] = deal(ce{:});

pixunit = median([diff(x),diff(y)]);
aspects = sort([pixunit*nx, pixunit*ny]);

return

function aspects = aspectparse(x,y,bw,lg)
try,
  bwrm = bwmorph(bwmorph(bw,'close'),'open');  
  if lg==1,
    bwrm = getLargestLabel(bwrm);
  end
  rp = regionprops(bwlabel(bwrm),{'MajorAxisLength','MinorAxisLength'});
  aspects = [rp.MinorAxisLength,rp.MajorAxisLength];
catch,
  aspects = [NaN,NaN];
end
return
