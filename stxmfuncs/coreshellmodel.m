%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~coreshellmodel.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [shellt, r, big, mini] = coreshellmodel(x0,r,Rbig,beta)

if isempty(r),
  r = linspace(0,Rbig,100);
end
k = x0(1);
s = x0(2);

Rmini = k*Rbig;
big = 2*Rbig*sin(acos(r/Rbig));
mini = ifelse(r <= Rmini, 2*Rmini*sin(acos(r/Rmini)),0);
shellt = beta * (big - s*mini); %% shell thickness

return
