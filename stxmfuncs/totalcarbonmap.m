%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~totalcarbonmap.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = totalcarbonmap(self)

preedge = self.stackarr(:,:,find(self.energy > 278 & self.energy < 283));
postedge = self.stackarr(:,:,find(self.energy >= 305 & self.energy < 320));
% preedge(~isfinite(preedge)) = NaN;
% postedge(~isfinite(postedge) = NaN;

self.bg = nanmean(preedge,3);
self.totalc = nanmean(postedge,3) - self.bg; 

return
