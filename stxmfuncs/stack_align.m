% adapted taken from zimba's zstack_align.pro, zstack_align_images.pro
% as noted in stack_doalign()
% does not do:
% - edge enhancement, edgegauss smoothing
% - tri-fit (polynomial fitting to improve shift with 3-point fit)
% - image shift is done through FourierShift2D (from m-file exchange)
%   rather than geometric warping (IDL's poly_2d function)
% calculated shifts are not the same even when aXis's edgegauss smoothing
% is turned off (needs more testing)

%%%_* ===== stack_align =====
function self = stack_align(self,refen)

reg = struct();
% currently, corr_dim_stack is not saved
[minval,refidx] = min(abs(self.energy-refen));
[self.stackarr,reg.xshift,reg.yshift,reg.corr_dim_stack,reg.corr_image_stack] = ...
    stack_doalign(self.stackarr,refidx) ;
self.reg = reg;

return

%%%_* ===== main function =====
function [stack,xshiftv,yshiftv,corr_dim_stack,corr_image_stack] = stack_doalign(stack,refidx) 
% Shift the image
% does not do:
% - edge enhancement, edgegauss smoothing
% - tri-fit (polynomial fitting to improve shift with 3-point fit)
% - image shift is done through FourierShift2D (from m-file exchange)
%   rather than geometric warping (IDL's poly_2d function)

shift_threshold = 0.01;

xshiftv = zeros(1,size(stack,3));
yshiftv = zeros(1,size(stack,3));
corr_dim_stack = zeros(2,size(stack,3));
corr_image_stack = zeros(size(stack));
for k = 1:size(stack,3),
    if k==refidx,
        continue
    end
    [xshift,yshift,corr_dims,corr_image] = ...
        align_image(stack(:,:, refidx),stack(:,:,k));
    xshiftv(k) = xshift;
    yshiftv(k) = yshift;
    corr_dim_stack(:,k) = corr_dims;
    corr_image_stack(:,:,k) = corr_image;
    if( abs(xshift) > shift_threshold | abs(yshift) > shift_threshold ),
        stack(:,:,k) = image_shift(stack(:,:,k),xshift,yshift);
    end
end

return

%%%_* ===== align individual images =====
function [xshift,yshift,corr_dims,corr_image] = align_image(image1,image2)
% image 1 is reference image
% does not actually align but returns parameters (xshift, yshift) 
% to be used for alignment

nx = size(image1,2); % number of columns in image (as enclosed by dragbox)
ny = size(image1,1); % number of rows in image (as enclosed by dragbox)
xcenter = 0;
ycenter = 0;

% $$$ maxshift = round(min(0.8*[nx/2,ny/2]));
maxshift = min([nx/2,ny/2,10]);

fft1_ = image1;
fft2_ = image2;

fft1_ = shift(fft1_,nx/2,ny/2);
fft1_ = ifft2(fft1_);
fft1_ = shift(fft1_,nx/2,ny/2);

fft2_ = shift(fft2_,nx/2,ny/2);
fft2_ = ifft2(fft2_);
fft2_ = shift(fft2_,nx/2,ny/2);

fft1_ = conj(fft1_);
fft1_ = fft2_.*fft1_;
fft1_ = shift(fft1_,nx/2,ny/2);
fft1_ = fft2(fft1_);
fft1_ = shift(fft1_,nx/2,ny/2);
fft1_ = abs(fft1_);

cleft  = max([(nx/2 - maxshift),0])+1;
cright = min([(nx/2 + maxshift),nx-1])+1;
cbot   = max([(ny/2 - maxshift),0])+1;
ctop   = min([(ny/2 + maxshift),ny-1])+1;

[max_fft1_,max_index] = max(elv(fft1_(cleft:cright,cbot:ctop)));
min_fft1_ = min(elv(fft1_(cleft:cright,cbot:ctop)));
xcenter = cleft+imod(max_index,cright-cleft+1); % check
ycenter = cbot+imod(max_index,ctop-cbot+1); %check
%% center of mass
cm_threshold = min_fft1_ + (0.5*(max_fft1_-min_fft1_));	% mean intensity of correlation function
cm_image = zeros(size(fft1_));
cm_indices=find(fft1_ > cm_threshold);
n_cm=length(cm_indices);
%%
cleft  = max([(xcenter - maxshift),0])+1;
cright = min([(xcenter + maxshift),nx-1])+1;
cbot   = max([(ycenter - maxshift),0])+1;
ctop   = min([(ycenter + maxshift),ny-1])+1;
cm_image(cleft:cright,cbot:ctop) = fft1_(cleft:cright,cbot:ctop);
cm_indices=find(cm_image > cm_threshold);
n_cm = length(cm_indices);

if n_cm > 4,
    xpositions = imod(cm_indices,nx);
    ypositions = floor((cm_indices-1)/nx+1); % index changed
    xcenter=total(xpositions.*fft1_(cm_indices)) / total(fft1_(cm_indices));
    ycenter=total(ypositions.*fft1_(cm_indices)) / total(fft1_(cm_indices));
% $$$ else,
    % what if n_cm is less than or equal to 4
    % align using 3-point quadratic fit of maximum of correlation
    %   function
    % -unimplemented-
% $$$     xpts = xcenter+[-1,0,1];
% $$$     ypts = fft1((xcenter-1):(xcenter+1),ycenter);
% $$$     [xfit,xpeak] = tri_fit(xpts,ypts);
% $$$     xpts = fft1(xcenter,(ycenter-1):(ycenter+1));
% $$$     ypts = ycenter+[-1,0,1];
% $$$     [yfit,ypeak] = tri_fit(ypts,xpts);
% $$$     xcenter = xpeak;
% $$$     ycenter = ypeak;
end

corr_image = fft1_;
corr_dims = [xcenter,ycenter];
xshift = xcenter - nx/2;
yshift = ycenter - ny/2;

return

%%%_* ===== supporting functions =====

function im = shift(im,xshift,yshift)
% wrapper for circhift in MATLAB

im = circshift(im,[yshift, xshift]);
% $$$ im = circshift(im,[xshift, yshift]);

return

function im = image_shift(im,xshift,yshift);
% shift image using Fourier Transform

im = FourierShift2D(im,[yshift xshift]);

return

function summed = total(x)
% sum all elements of array

summed = sum(elv(x));

return

function y = elv(x)
% elongate in vertical direction

y = reshape(x,[],1);

return

function value = imod(x,y)
% used in place of mod in IDL routine to account for the 
% 1-indexing used by MATLAB (IDL uses 0-indexing of arrays)

value = mod(x-1,y)+1;
    
return
