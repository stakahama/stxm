%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~crop2xy.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = crop2xy(self)

if self.nregions == 1,
    self.stackarr = crop1(self.stackarr,self);
else,
    for j=1:self.nregions,
        self.multistacks{j} = crop1(self.multistacks{j},self.xyregions{j});
    end
end

return

function cr = crop1(arr,xy)

cr = arr(1:length(xy.ycoord),1:length(xy.xcoord),:);

return
