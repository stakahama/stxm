%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~enhance.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = enhance(self,on)
if nargin < 2, on = 0; end
if ~on, return; end

if self.nregions == 1,
    self.stackarr = enhancesinglestack(self.stackarr,on);
else,
    for j = 1:self.nregions,
        self.multistacks{j} = enhancesinglestack(self.multistacks{j},on);
    end
end

return

function stackarr = enhancesinglestack(stackarr,on)
if on,
  %% threshold
  BW = repmat(0,arrSubset(size(stackarr),1:2));
  for i = 1:size(stackarr,3),
    %% add medfilt2 optional
    grayimage = mat2gray(stackarr(:,:,i));
    stackarr(:,:,i) = histeq(gray2ind(grayimage));
  end
end

return
