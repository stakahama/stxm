%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~watershedtransf.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function wat = watershedtransf(im,bw,dotrim,val),
if nargin < 4 | length(val) > 1,
  dotype = 'enhance'; 
  if ~exist('val'),
    val = 1:9;
  end
else,
  dotype = 'watershed'; 
end

if dotrim,
  [a,b,c] = trimnan(im,bw);
else,
  a = im; c = bw; a(isnan(a)) = 0;
end

se = strel('disk', 15);
% se = strel('disk', 9);

g = gray2ind(mat2gray(a));
Itop = imtophat(g,se);
Ibot = imbothat(g,se);
Ienhance = imsubtract(imadd(Itop, g), Ibot);
Iec = imcomplement(Ienhance);


if strcmp(dotype,'enhance'),
  ct = 0;  
  for i = val,
    ct = ct + 1;
    j = i*10;
    subplot(3,3,ct);
    I = imextendedmin(Iec,j);
    imagesc(I);
    title(j);
  end
  wat = [];  return
else,
  Iemin = imextendedmin(Iec, val);
  Iimpose = imimposemin(Iec, Iemin | bwperim(c) );
  Iimpose(~c) = -Inf;  
  wat = watershed(Iimpose);
  return
end

