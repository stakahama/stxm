%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~fitradial.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [beta,R,fitted] = fitradial(radialdist,opdens,bwimg,perimpts,intercept)

if nargin < 5,
  intercept = 1;
end

r = radialdist(find(bwimg));
od = opdens(find(bwimg));
if nargin < 4,
  R = max(r);  
else,
  if isempty(perimpts),
    R = max(r);
  else,
    R = mean(radialdist(find(perimpts)));  
  end
end
d = R .* sin(acos(r(:)./R));
od(~cellfun('isreal',num2cell(d)) | ~isfinite(d)) = NaN;

if any(isnan(od)),
  r = r(~isnan(od));  
  d = d(~isnan(od));
  od = od(~isnan(od));
end

if intercept,
  [coeffs{1:5}] = regress(od,[ones(size(d)),d]);
  beta = coeffs([1,5]);
  i = getElem(r,@sort,2);
  fitted = {r(i),beta{1}(1)+beta{1}(2)*d(i)};
else,  
  [coeffs{1:5}] = regress(od,d);  
  beta = coeffs([1,5]);
  i = getElem(r,@sort,2);
  fitted = {r(i),beta{1}*d(i)};
end

return
