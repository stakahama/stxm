%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~loadstack.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = loadstack(self,ximfiles)

if self.nregions==1,
    self.stackarr = loadsinglestack(ximfiles);
else,
    regs = regexprep(ximfiles,'\w+\_\w+(\d)\.xim$','$1','tokenize')';    
    sets = rsplit(ximfiles,regs);
    rstck = cell(length(sets),1);
    for j = 1:length(sets),
        rstck{j} = loadsinglestack(sets(j).data);
    end
    self.multistacks = rstck;
end

return

function stackarr = loadsinglestack(ximfiles)

arr = dlmread(ximfiles{1});
stackarr = repmat(NaN,[size(arr),length(ximfiles)]);
stackarr(:,:,1) = arr; 
for i=2:length(ximfiles),
    stackarr(:,:,i) = dlmread(ximfiles{i});
end

return
