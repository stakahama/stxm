%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~myimagesc.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function myimagesc(self,im);

imagesc(self.xcoord,self.ycoord,im);
set(gca,'DataAspectRatio',[1,1,1],'YDir','normal');

return
