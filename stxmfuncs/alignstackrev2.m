%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~alignstackrev2.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = alignstackrev2(self,eV,frac)
%% revision of stackalign
if nargin < 3, frac = 0.75; end
refimage = self.stackarr(:,:,getElem(abs(self.energy-eV),@min,2));
for i=1:size(self.stackarr,3),
    self.stackarr(:,:,i) = alignImages(self.stackarr(:,:,i),refimage,frac);
end

return

function recovered = alignImages(img,refimg,f)
% 2-D alignment by cross-correlation
% img scaled by f and correlated to refimg (which is larger than size(b)*f)
% does not crop img but rather expands the canvas of refimg
% fills NaNs with median filter values

if nargin < 3, f = 0.75; end
if isempty(f), f = 0.75; end

%% expand refimg frame
bordersize = ceil(size(refimg)*(f^-1 - 1)/2);
bigref = zeros(size(refimg) + bordersize*2);
bigref((bordersize(1)+1):(end-bordersize(1)),...
    (bordersize(2)+1):(end-bordersize(2))) = refimg;
bigref = fillNaNs2(bigref);
img = fillNaNs2(img);

%% cross correlation
c = normxcorr2(img,bigref);
% figure, surf(c), shading flat
[max_c,imax] = max(abs(c(:)));
[ypeak,xpeak]= ind2sub(size(c),imax(1));
offset = [(xpeak-size(img,2)),(ypeak-size(img,1))] - bordersize;

%% indices
xi = (1:size(bigref,2))+offset(1);
yi = (1:size(bigref,1))+offset(2);
xi = xi(find(xi >= 1 & xi <= size(bigref,2)));
yi = yi(find(yi >= 1 & yi <= size(bigref,1)));

if(offset(1) >= 0), x2i = 1:length(xi);
else, x2i = (size(bigref,2)-length(xi)+1):size(bigref,2); end
if(offset(2) >= 0), y2i = 1:length(yi);
else, y2i = (size(bigref,1)-length(yi)+1):size(bigref,1); end

%% recover image
bigimg = repmat(NaN,size(bigref));
bigimg((bordersize(1)+1):(end-bordersize(1)),...
    (bordersize(2)+1):(end-bordersize(2))) = img;
recovered = repmat(NaN,size(bigref));
recovered(yi,xi) = bigimg(y2i,x2i);

recovered = recovered((bordersize(1)+1):(end-bordersize(1)),...
    (bordersize(2)+1):(end-bordersize(2)));

return

function arr = fillNaNs(arr)

funcs = shapemetrics;
bw = ~isfinite(arr);
[xmat,ymat] = feval(funcs.coordmats,1:size(bw,1),1:size(bw,2));
xm = xmat; ym = ymat;
xm(bw) = NaN; ym(bw) = NaN;
indices = repmat(NaN,sum(bw(:)),1);
ct = 1;
for pix = find(bw)',
    indices(ct) = ...
        getElem(sqrt((xmat(pix) - xm(:)).^2 + (ymat(pix) - ym(:)).^2),@nanmin,2);
    ct = ct + 1;
end
arr(bw(:)) = arr(indices);

return

function arr = fillNaNs2(arr)

filtered = medfilt2(arr,ceil(size(arr)*0.25));
arr(~isfinite(arr)) = filtered(~isfinite(arr));

return
