%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~alignstackrev3.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

% If A = reference image and B = new image,
% image B is trimmed to 'frac' parameter and normalized cross-correlation
% is computed. The location of the maximum value is used to determine the
% amount by which the image should be shifted (translated)
% If the true control point is outside the trimmed region, this algorithm
% will not perform well.

function self = alignstackrev3(self,eV,frac)
%% revision of stackalign

if nargin < 3, frac = 0.75; end

if self.nregions ==1,
    self.stackarr = alignsinglestack(self.stackarr,self.energy,eV,frac);
else,
    for j = 1:self.nregions,
        if max(self.xyregions{j}.xcoord) * ...
                max(self.xyregions{j}.xcoord) < 1.05^2,
            continue
        end
        self.multistacks{j} = ...
            alignsinglestack(self.multistacks{j},self.energy,eV,frac);
    end
end
return

function arr = alignsinglestack(arr,energy,refeV,frac)
%% crop rows and columns that are all NaNs
%% add medfilt2 optional
try,
  refimage = arr(:,:,getElem(abs(energy-refeV),@min,2));
  for i=1:size(arr,3),
    img = arr(:,:,i);
    cropped = crop3D(reshape([img(:),refimage(:)],[size(img),2]));
    arr(:,:,i) = alignImages(cropped(:,:,1),cropped(:,:,2),...
      arr(:,:,i),frac);
  end
end

return

function recovered = alignImages(croppedimg,croppedref,img,f)
% 2-D alignment by cross-correlation
% croppedimg scaled by f and correlated to croppedref (which is larger than size(b)*f)
% croppedimg and croppedref have NaNs along edges trimmed off

if nargin < 3, f = 0.75; end
if isempty(f), f = 0.75; end

%% crop
trimsize = floor(size(croppedimg) * (1-f) /2)-1;
ct = 0;
while (1+trimsize(1)) < 1 & (size(croppedimg,1)-trimsize(1)) > size(croppedimg,1) | ...
    ct > 100,
  f = f - .05;
  trimsize = floor(size(croppedimg) * (1-f) /2)-1;  
  ct = ct + 1;
end
small = croppedimg((1+trimsize(1)):(size(croppedimg,1)-trimsize(1)),...
    (1+trimsize(2)):(size(croppedimg,2)-trimsize(2)));

%% cross correlation
c = normxcorr2(small,croppedref);
% figure, surf(c), shading flat
[max_c,imax] = max(abs(c(:)));
[ypeak,xpeak]= ind2sub(size(c),imax(1));
offset = [(xpeak-size(small,2)),(ypeak-size(small,1))] - trimsize;

%% indices
xi = (1:size(img,2))+offset(1);
yi = (1:size(img,1))+offset(2);
xi = xi(find(xi >= 1 & xi <= size(img,2)));
yi = yi(find(yi >= 1 & yi <= size(img,1)));

if(offset(1) >= 0), x2i = 1:length(xi);
else, x2i = (size(img,2)-length(xi)+1):size(img,2); end
if(offset(2) >= 0), y2i = 1:length(yi);
else, y2i = (size(img,1)-length(yi)+1):size(img,1); end

%% recover image
recovered = repmat(NaN,size(img));
recovered(yi,xi) = img(y2i,x2i);

return
