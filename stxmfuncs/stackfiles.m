%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~stackfiles.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [hdrfile,ximfiles] = stackfiles(folder)

contents = dir(folder);
contents = contents(cellfun('isempty',...
    regexp({contents.name},'[#~]')));
ext = cell(length(contents),1);
for i=1:length(contents),
    ext{i} = getElem(contents(i).name,@fileparts,3);
end
ximfiles = {contents(strmatch('.xim',ext)).name};

hdrfile = contents(strmatch('.hdr',ext)).name;

return
