%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getbw.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [BW,thres] = getbw(localoption,imgarr,method,badPnts,morphf,op)
% localoption = 'doall' or otherwise single image
% imgarr = stack array or image
% method = 'Otsu' or 'kmeans'
% badPnts = indices of bad pixels
% morphf = morphological function
% op = operator for 'doall', either '+' (union) or '-' (intersection)
if nargin < 6,
  op = 'union';
end

%% ++++++++++++++++++++++++++++++++++
%% Filter and threshold
if ~isequal(localoption,'doall'),
  [BW,thres] = filtandthresh(imgarr);    
else,
  if strcmp(op,'union')==1,  
    BW = zeros(arrSubset(size(imgarr),1:2));
    thres = NaN;    
    for i = 1:size(imgarr,3),
      
      BW = BW + filtandthresh(imgarr(:,:,i));
    end
    BW = logical(BW);
  elseif strcmp(op,'intersection')==1,
    BW = logical(repmat(1,arrSubset(size(imgarr),1:2)));
    thres = NaN;
    for i = 1:size(imgarr,3),
      BW = BW & logical(filtandthresh(imgarr(:,:,i)));
    end
  end
end

%% Remove bad points
if length(badPnts) > 0,
    try, BW(badPnts) = 0; end
end

% original, as of 7/29/09
if ~isempty(morphf), % 'open' or 'clean'
  BW = bwmorph(BW,morphf);
end
BW = bwareaopen(BW,16);
BW = bwmorph(BW,'fill');

% removes final opening and filling by default:
% if ~isempty(morphf), % 'open' or 'clean'
%   BW = bwmorph(BW,morphf);
% else,
%   BW = bwareaopen(BW,16);
%   BW = bwmorph(BW,'fill');
% end

return


%% ====================================================
function [BW,thres] = filtandthresh(img,method)

if nargin < 2,
    method = 'Otsu';    
end
% Median filter (already done)
% img(~isfinite(img)) = NaN;
% im2 = medfilt2(img);
im2 = img;

% Thresholding
if isequal(method,'Otsu')
	% Without using Gaussian Blur
    thres = graythresh(im2);
	BW = im2bw(im2,thres);
elseif isequal(method,'Kmeans'),
    ysub = reshape(im(isfinite(im)),[],1);     
    IDX = kmeans(ysub,2,'Replicates',10);
    thres = max([min(ysub(find(IDX==1))),min(ysub(find(IDX==2)))]);  
    BW = im >= thres;
end

return
