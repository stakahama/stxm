%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getdiameter.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = getdiameter(self,im)
% im is name of BW image

shapefuncs = shapemetrics;
self.diameter = feval(shapefuncs.equivSphereRadius,...
        self.xcoord,self.ycoord,self.(im)).*2;
    
return
