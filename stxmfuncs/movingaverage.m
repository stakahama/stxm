%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~movingaverage.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function y=movingaverage(x,m)

n=length(x);
z = [0 cumsum(x)];
y = ( z(m+1:n+1) - z(1:n-m+1) ) / m;

return
