%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~imthresh1.m~
% $Rev: 11 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function self = imthresh1(self,im,method,badPnts)

if nargin < 3, method = 'Otsu'; end
if isa(im,'char'),
    img = self.(im);
else
    img = self.stackarr(:,:,getElem(abs(self.energy-im),@min,2));
end

% Median filter
img(~isfinite(img)) = NaN;
im2 = medfilt2(img);

% Thresholding
if isequal(method,'Otsu')
	% Without using Gaussian Blur
    thres = graythresh(im2);
	BW = im2bw(im2,thres);
elseif isequal(method,'Kmeans'),
    ysub = reshape(im(isfinite(im)),[],1);     
    IDX = kmeans(ysub,2,'Replicates',10);
    thres = max([min(ysub(find(IDX==1))),min(ysub(find(IDX==2)))]);  
    BW = im >= thres;
end
try, BW(badPnts) = 0; end
    
% Labelling
newBW = getLargestLabel(BW);

% Average Spectra
x = reshape(self.stackarr,size(self.stackarr,1)*size(self.stackarr,2),...
    size(self.stackarr,3));    
x(~isfinite(x)) = NaN;
aveSpec = nanmean(x(find(newBW),:))';

self.thres = thres;
self.bw = newBW;
self.avespec = aveSpec;

return

