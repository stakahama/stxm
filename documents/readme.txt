=================================
2/10/08 - Version 2

files included -

main_prog.m
makeplots.m (called by main_prog.m)
fitspectra.m - peakfitting of average spectra
export_diamspectra.m (optional)
getshapes.m (optional)

run main_prog.m
then fitspectra.m

=================================
2/18/09 - Version 3

now can handle stacks with multiple regions (over 50% of archived
stacks had multiple regions). 

[multiple regions: single energy but multiple XY regions.]

accomplished by merging them horizontally onto a single image (after
2D interpolation to the highest resolution).

=================================
09/01/09 - Version 3 for distribution

multiple region mode turned off by default (inquire if desired).
assumes current directory will be the directory that contains the .m
file you are running.
