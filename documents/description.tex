\documentclass{article}
\usepackage{fullpage}
\usepackage{fancyvrb}
\usepackage{color}
\usepackage{hyperref}
\renewcommand*{\sectionautorefname}{Section}
\renewcommand*{\subsectionautorefname}{Subsection}
\newcommand{\HRule}{\medskip\rule{\textwidth}{0.5pt}}
\newcommand{\minip}[1]{\begin{minipage}[t]{.65\textwidth}#1\end{minipage}}
\parindent 0pt
\title{STXM-NEXAFS stack analysis with stxm m-files}
\author{S. Takahama}
\begin{document}
\maketitle

This document describes how to generate and extract information from MAT-files (extension: \verb=.mat=), starting with a directory containing subdirectories of \verb=.xim= and \verb=.hdr= files. 

\section{Scripts}

Most script files except \verb=export_forpmf.m= read in \verb=userinputs.m= (\autoref{sec:userinputs}).

\begin{enumerate}
\item \verb=main_prog.m= --- Convert \verb=.xim= and \verb=.hdr= files to MAT-file (\autoref{sec:stack}).
\item \verb=getavespec.m= --- Calculate average spectra; store back in MAT-files.
\item \verb=plot_particles.m= --- Generate png image figures. (requires \verb=getavespec.m= to be run a priori)
\item \verb=fit_export_plot_avefit.m= --- Fit peaks and 1) save back to MAT-files, 2) export text file containing fitting parameters. (requires \verb=getavespec.m= to be run a priori)
\item \verb=export_diamspectra.m= (optional) --- Export diameter and spectra of particles (for cluster/factor analysis).
\item \verb=export_aspectratio.m= (optional) --- Calculate and export aspect ratio for each particle.
\item \verb=fit_plot_pixelfit.m= --- peakfits individual pixel spectra.
\item \verb=plot_edges_radial= --- plots aniosotropy of functional group distribution in HSV color coordinates (requires \verb=fit_plot_pixelfit.m= to be run a priori).
\item \verb=fit_export_edgetest_callR.m= --- hypothesis testing compares layered model vs base case (single---parameter) model using BIC scoring (requires \verb=fit_plot_pixelfit.m= to be run a priori; also requires \href{http://cran.r-project.org}{the R software program} to be installed).
\item \verb=export_forpmf.m= --- standalone script; to be used in conjunction with \href{https://github.com/stakahama/pmf}{pmftools} which does not use \verb=userinputs.m=.
\end{enumerate}

\emph{For OS X and Linux (but possibly not Windows)}: To execute these scripts, I have a script in my \verb=~/bin/= directory called \verb=mscript=, the contents of which are as follows:
\begin{Verbatim}[formatcom=\color{blue}]
#!/bin/bash
matlabexec=/Applications/MATLAB_R2012a.app/bin/matlab
mfile=$(echo $1 | sed -e 's/\.m$//g')
$matlabexec -nodesktop -nosplash -r "$mfile;exit" -logfile output_file
\end{Verbatim}
To run the program, I edit \verb=userinputs.m= and type in a terminal (or terminal emulator like Emacs eshell):
\begin{Verbatim}[formatcom=\color{blue}]
$ ~/bin/mscript filename.m
\end{Verbatim}
(\verb=~/bin/= prefix is not necessary if it is defined in \verb=$PATH=). Note that MATLAB license must be active.

\section{User inputs}
\label{sec:userinputs}

Example contents of \verb=userinputs.m=:
\begin{Verbatim}[formatcom=\color{blue}]
stackdir = '../../data/C-edge_stacks';
matdir = 'matfiles';
outdir = 'outputs'; mkdir(outdir);
refen = 288.85; %eV
numen = 50; %95
fitmethod = 4; % peakfitting method {1,2,3,4,5}
regmethod = 1; % registration method {1,2,3,4}

% file permission; http://www.mathworks.ch/help/techdoc/ref/fopen.html
fperm = 'a'; % usually 'w' or 'a'

% overwrite (logical) variable used by for 
% plot_particles.m, fit_export_plot_avefit.m, fit_plot_pixelfit.m
overwrite = 0; % False

% for edge-fitting (7:COOH, 4:C=C) used by 
% fit_export_edgetest_callR.m
edgefeature = [4 7]; 
\end{Verbatim}

\section{Stack Structure}
\label{sec:stack}

Each STXM stack is stored as a data structure (\verb=structure=) in its own MAT-file. Matlab does not have facilities for user-defined objects and methods; the \verb=.= (dot) operator is  used to access attributes. Auxiliary scripts are provided to extract and manipulate the extracted data, but this document describes the attributes or fields of this data structure. To get/extract the value contained in a field, 'myfield' in structure 'mystructure', use the following syntax:

\begin{Verbatim}[formatcom=\color{blue}]
mystructure.myfield
mystructure.('myfield')
\end{Verbatim}

% \HRule
\subsection{Stacks}
For STXM stack structures generated by \verb=main_prog_merge.m=, each stack is stored in a structure called \verb=S= and saved in individual \verb=.mat= files on the hard disk. Example usage:
\begin{Verbatim}[formatcom=\color{blue}]
>> load newmatfiles/60610056.mat
>> S

S = 

          name: '60610056'
        energy: [1x95 double]
        xcoord: [1x20 double]
        ycoord: [1x20 double]
      stackarr: [20x20x95 double]
            bg: [20x20 double]
        totalc: [20x20 double]
         thres: 0.0431
       ccoeffs: [20x20x15 double]
     particles: [1x1 struct]
    coeffnames: {1x15 cell}
\end{Verbatim}

Description of fields:
\begin{center}
\begin{tabular}{ll}
Field & Description \\
\hline
\verb=name= & string; stack name\\
\verb=energy= & 1-D array of energy (eV)\\
\verb=xcoord= & 1-D array of x-coordinates ($\mu$m)\\
\verb=ycoord= & 1-D array of y-coordinates ($\mu$m)\\
\verb=stackarr= & 3-D array of absorbances ($x \times y \times \textrm{energy}$)\\
\verb=bg= & 2-D array of pre-edge absorbance ($x \times y$)\\
\verb=totalc= & 2-D array of post-edge absorbance ($x \times y$)\\
\verb=thres= & scalar value; threshold value for image segmentation\\
\verb=ccoeffs= & 3-D array of fitted coefficients ($x \times y \times \textrm{coefficients}$)\\
\verb=particles= & structure$\dagger$\\
\verb=coeffnames= & 1-D cell array of coefficient names \\
\end{tabular}
\end{center}

${}^\dagger$Exploring the \verb=particle= structure - see \autoref{sec:particles}.

% \HRule
\subsection{Particles within stacks}
\label{sec:particles}
\begin{Verbatim}[formatcom=\color{blue}]
>> S.particles(1)

ans = 

              p288: 0
             bw288: [20x20 logical]
          diameter: 0.2220
    carbon_regions: [1x1 struct]
              name: '60610056_p1'
                bw: [20x20 logical]
           avespec: [1x95 double]
        aveccoeffs: [1x14 double]
        coeffnames: {1x14 cell}
              kval: 0.1496
             kspec: [1x95 double]
           kbounds: [296 300.0010]
\end{Verbatim}

\begin{center}
\begin{tabular}{ll}
Field & Description \\
\hline
\verb=p288= & integer; index as determined by 288 image\\
\verb=bw288= & 2-D logical array; foreground and background as determined by 288 image \\
\verb=diameter= & circle-equivalent diameter ($\mu$m) \\
\verb=carbon_regions= & structure$\ddagger$ \\
\verb=name= & string; name of particle within stack \\
\verb=bw= & \minip{2-D logical array; foreground and background as determined by total carbon image (union of individual carbon regions)}\\
\verb=avespec= & spectra averaged over all carbon regions\\
\verb=aveccoeffs= & fitted coefficients for average spectra\\
\verb=coeffnames= & 1-D cell array of coefficient names\\
\verb=kval= & scalar value; integrated absorbance of potassium  \\
\verb=kspec= & 1-D array; baselined spectrum of potassium region\\
\verb=kbounds= & 1-D array; range of energies over which potassium absorbance is integrated\\
\end{tabular}
\end{center}

${}^\ddagger$Exploring the \verb=carbon_regions= structure - see \autoref{sec:carbon}.

% \HRule
\subsection{Carbon regions within particles}
\label{sec:carbon}
\begin{Verbatim}[formatcom=\color{blue}]
>> S.particles(1).carbon_regions(1)

ans = 

    regnum: 1
        bw: [20x20 logical]
\end{Verbatim}

\begin{center}
  \begin{tabular}{ll}
    Field & Description \\
    \hline
    \verb=regnum= & integer; region number\\
    \verb=bw= & \minip{2-D logical array; foreground and background as determined by total carbon, corresponding to regnum}\\
  \end{tabular}
\end{center}

% \section{Functions}

\end{document}