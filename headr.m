%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~headr.m~
% $Rev: 8 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

%% change location of funclib and stfuncs
%% currently specified relative to cdir

funclib = fullfile(pwd,'stxmfuncs');
genfuncs = fullfile(pwd,'addfuncs');
addpath(funclib);
addpath(genpath(genfuncs));
clear funclib, genfuncs;
