%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~main_prog_merge.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

% $$$ main program
% $$$ output is set of .mat files in newmatfiles/
% $$$   containing information on each particle
% $$$   + records to failedstacks.txt (possibly no particle found)
% $$$ saves mat objects to newmatfiles

clear all; close all;
% $$$ cdir = '/Users/stakahama/tmp/stxmv3';
% $$$ cd(cdir);
cdir = pwd;
run headr;
run userinputs;
warning off all

% $$$ where your stacks are:
%% now defined in userinputs.m
% $$$ stackdir = '../../data/stxm/xim';
if exist('stacksubset.m','file'),
    disp('------------------------------------')
    disp('-----READING FROM stacksubset.m-----')
    disp('------------------------------------')    
    run stacksubset; % creates 'stacklist' in global space
    stacks = cell2struct(stacklist,'name');
    clear stacklist;
else,
    stacks = dir(stackdir);
end

mkdir(matdir)
matfiles = dir(matdir);
nomat = inline('strrep(x,''.mat'','''')','x');
if(length(matfiles)<=3)
    matfiles = {nomat(matfiles(3).name)};
else,
    matfiles = mapfuni(nomat,...
                       arrSubset({matfiles.name},...
                                 3:length(matfiles)));
end

ct = 0;
fout = fopen(fullfile(outdir,sprintf('failedstacks.txt')),fperm); 
for i = 1:length(stacks),

  stackname = stacks(i).name;
  if ~overwrite & (~isempty(strmatch('.',stackname)) | ~isempty(strmatch(stackname,matfiles))),  
    continue
  end
  ct = ct + 1;
% $$$   if ct > 3, break; end
  
  disp(sprintf('%d: %s',i,stackname));         
  try,
    folder = fullfile(stackdir,stacks(i).name);
    [hdrfile,ximfiles] = stackfiles(folder);
    %% === load file ===
    cd(folder);
    S = readhdr(hdrfile);
    S = loadstack(S,ximfiles);
    cd(cdir);
    %% === process image  === 
    S = normalizecoord(S);      
    S = crop2xy(S);      
    S = enhance(S,0);
    S = despeckle(S);
    S = mergestacks(S,1);    
    %%  === failed === 
    if length(S.energy) < numen,
      fprintf(fout,'%s,en: %d\n',S.name,length(S.energy));
      disp(sprintf('en: %d',length(S.energy)));      
      continue
    end
    switch regmethod
      case 1
        trimfrac = 0.9;
        S = alignstackrev3(S,refen,trimfrac);
      case 2
        S = stackregister(S,refen);
      case 3
        S = ft_align_stack(S,refen);
      case 4
        S = stack_align(S,refen);
      otherwise
        disp('-no alignment-');
    end
    S = trans2absor(S,1,refen);
    S = totalcarbonmap(S);
    
    %% === partition === 
    nc = imthreshk(S,'totalc','Otsu',{},'','','count');
    disp(sprintf('nc: %d',length(nc)));
    if isempty(nc),
      feval(plotfun.failedplots,S,'plots','_failed')      
      fprintf(fout,'%s: no particle',S.name);
      disp('empty');
      continue
    end
    if length(nc) > 5, nc = nc(1:5); end

    %% if n > 1, more than one particle exists in image
    %% loop through each particle
    %% find associated 288 image
    allc = {}; 
    for p = nc,                                                     
      Sc = addfds(struct,S,{'energy','stackarr','totalc'}); % copy
      Sc.regnum = p;
      Sc = imthreshk(Sc,'totalc','Otsu',{'bw','thres','avespec'}, p,'');
      Sc = imthreshk(Sc,refen,'Otsu',{'bw288','p288'},Sc.bw,'');
      allc = [allc,{Sc}];
    end
    
    %% === merge carbon if associated with same 288 image === 
    plist = [];
    for j=1:length(allc),
      Sc = allc{j};
      pnew = Sc.p288;
      %% this is new:
      if pnew==0, continue; end
      %%
      regdata = struct('regnum',Sc.regnum,'bw',Sc.bw);
      if all(pnew ~= plist),
        k = length(plist) + 1;
        if k==1,
          particles = struct('name','','p288',[],'bw288',[],...
                             'diameter',[],'bw',[],...
                             'carbon_regions',{});
        end
        diam = 	getdiameter(addfds(Sc,S,{'xcoord','ycoord'}),'bw288');
        particles(k) = struct('name','','p288',pnew,...
                              'bw288',Sc.bw288,'diameter', ...
                              diam.diameter,'bw',regdata.bw,...
                              'carbon_regions',regdata);  
        clear diam
        plist = [plist,pnew];    
      else,
        k = find(pnew == plist);
        m = length(particles(k).carbon_regions)+1;
        particles(k).carbon_regions(m) = regdata;
        particles(k).bw = particles(k).bw + regdata.bw;
      end
      clear regdata Sc pnew 
    end
    
    %% === find emtpy === 
    emp = [];
    for k = 1:length(particles),
      if isempty(particles(k).carbon_regions), emp = [emp,k]; end
    end
    %% remove empty
    if length(emp) > 0,
      particles(emp) = [];  
    end
    %% label with name
    for k = 1:length(particles),
      particles(k).name = sprintf('%s_p%d',stackname,k);
      disp(particles(k).name)
    end
    S = setfield(S,'particles',particles);
    save(fullfile(matdir,stackname),'S');
  catch,
    cd(cdir);
    fprintf(fout,'%s\n',stackname);    
  end
  clear S particles
end
fclose(fout);
