
% $$$ stackdir = '../../data/C-edge_quick_stacks';
stackdir = '../../data/C-edge_stacks';
matdir = 'matfiles';
outdir = 'outputs'; mkdir(outdir);
refen = 288.85; % 
numen = 95; %50
fitmethod = 4; % peakfitting method {1,2,3,4,5}
regmethod = 1; % registration method {1,2,3,4}

% file permission
% http://www.mathworks.ch/help/techdoc/ref/fopen.html
fperm = 'a'; % usually 'w' or 'a'

% overwrite (logical) variable used by for 
% main_prog_merge, main_prog_noalign, plot_particles.m, fit_export_plot_avefit.m, fit_plot_pixelfit.m
overwrite = 1; % 0=False, 1=True

% for edgefitting (7:COOH, 4:C=C) for fitmethod==4
edgefeature = [4 7]; 
