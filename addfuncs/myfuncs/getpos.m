%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getpos.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function apos = getpos(gcf,gca,ismap);
% Arguments: gcf, gca
% +++ Call before setting 'DataAspectRatio' +++
% For cases when 'DataAspectRatio' is set to [1,1,1],
% returns actual boundaries ('Position') of new axes

fpos = get(gcf,'Position');
apos = get(gca,'Position');
dar = get(gca,'DataAspectRatio');
if nargin < 3,
    ismap = NaN;
end
%if ~isnan(ismap),
%    dar(1:2) = dar(2:3);
%end
centre = [apos(1)+apos(3)/2,apos(2)+apos(4)/2];
if dar(1)==dar(2)
    return;
elseif dar(1) > dar(2)
    newdy = apos(3)*dar(2)/dar(1)*fpos(3)/fpos(4);
    apos = [apos(1) centre(2)-newdy/2 apos(3) newdy];
else
    newdx = apos(4)*dar(1)/dar(2)*fpos(4)/fpos(3);
    apos = [centre(1)-newdx/2 apos(2) newdx apos(4)];
end
