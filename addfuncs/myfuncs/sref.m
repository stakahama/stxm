%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~sref.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function A = sref(X,i);
% structure referencing
% X,i

nms = fieldnames(X);
A = eval(strcat(char(X),'.',nms(i)));
