%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~rsplit.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function slist = rsplit(X,f,levels)
% rsplit(X,f,levels)
% X is cell, struct, or vector
% f is cell or numeric vector
% levels

nms = unique(f);
if isequal(class(nms),'double'),
    nms = mapfuni(@num2str,num2cell(nms));
    f = mapfuni(@num2str,num2cell(f));
end
slist = {};
for i=1:length(nms);
    if tf(strmatch('struct',class(X))) | tf(strmatch('cell',class(X))),
        slist{i} = X(strmatch(nms{i},f,'exact'));
    elseif tf(strmatch('double',class(X))),
        if all(size(X) > 1),
            slist{i} = X(strmatch(nms{i},f,'exact'),:);
        else 
            slist{i} = X(strmatch(nms{i},f,'exact'));
        end
    end
end

if nargin > 2,
	if isequal(class(levels),'double'),
        levels = mapfuni(@num2str,num2cell(levels));
	end
	tmpc = cell(size(levels));
	for i=1:length(nms),
        j = strmatch(nms{i},levels,'exact');
        tmpc{j} = slist{i};
	end
	slist = tmpc;
    nms = levels;
end

slist = struct('group',nms','data',slist');

return

function y = tf(x)

if isempty(x), 
    y=0;
else,
    y=x;
end
