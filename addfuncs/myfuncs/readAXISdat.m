%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~readAXISdat.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function S = readAXISdat(filenm)
f = fopen(filenm,'r');
temp = fscanf(f,'%f',8); fclose(f);
temp(3) = [];
S = cell2struct(num2cell(temp),{'nXpixels','nYpixels','minX','maxX','minY','maxY','nEnergies'},1);
return
