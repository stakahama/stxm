%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getarg.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function varargout = getarg(f,x,i);
% c = getarg(f,x,i)
% f is function handle
% x is argument (as array or cell)

for k=1:100;
    try,
        if iscell(x),
            [C{1:k}] = feval(f,x{:});
            Csave = C;
        else,
            [C{1:k}] = feval(f,x);
            Csave = C;
        end
    catch,
        break;
    end
end
varargout = Csave(i);
