%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~count_fields.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function fds = count_fields(fid,sep);
% requires split() function (Exchange)

if ~isnumeric(fid)
    fname = fid;
    fid = fopen(fname,'rt');
end

if nargin < 2
    sep = '\t';
end

if sep == '\t';
    delim = 9;
elseif sep == '\n';
    delim = 10;
elseif sep == ',';
    delim = ',';
else
    delim = 32;
end

x=0;
fds = [];
while(x~=(-1))
    x = fgetl(fid)
    y = split(delim,x)
    fds = [fds length(y)]
end
fds(length(fds)) = [];

try
    frewind(fid);
end
