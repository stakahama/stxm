%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~interpmap.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function s = interpmap(x,y,M,xnew,ynew)
% interpmap(x,y,M,xnew,ynew)
% alternatively, interpmap(x,y,M,f)
% default of f is 2
% returns interpolated map based on scaling factor f
% structure elements: x,y,map

if nargin < 4,
    f = 2;
elseif nargin < 5,
    f = xnew;
else,
    f = -1;
end
%
if f > 0,
	xnew = linspace(x(1),x(end),length(x)*f);
	ynew = linspace(y(1),y(end),length(y)*f);
end
%
[X,Y] = meshgrid(x,y);
[Xnew,Ynew] = meshgrid(xnew,ynew);
Mnew = interp2(X,Y,M',Xnew,Ynew)';
s = struct('x',xnew,'y',ynew,'map',Mnew);
