%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~rrange.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function y = rrange(x,narm)

if nargin < 2,
  y = [min(x),max(x)];
else
  y = [nanmin(x),nanmax(x)];
end
