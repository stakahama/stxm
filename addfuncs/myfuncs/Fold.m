%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~Fold.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function x = Fold(f,x,L)

for i=1:length(L),
    x = feval(f,x,L{i});
end
