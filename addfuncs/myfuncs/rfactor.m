%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~rfactor.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function Y = rfactor(X,Full,Order)
% Y = rfactor(X,Full,Order)

if nargin < 2, Full = X; end
q1 = sort(unique(Full));
if nargin < 3, Order = 1:length(q1); end 
if ~isnumeric(Order) & iscell(Order)
    z = zeros(length(Order),1);
	for i = 1:length(Order)
        z(i) = strmatch(Order(i),q1);
	end
    Order = z;
end
q1 = q1(Order);
Y = zeros(length(X),1);
for i = 1:length(q1)
    Y(strmatch(q1{i},X,'exact')) = i;
end

Y = struct('number',Y,'labels',struct('int',num2cell(1:length(q1))','name',q1'));
