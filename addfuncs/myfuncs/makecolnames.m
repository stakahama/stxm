%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~makecolnames.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function colnames = makecolnames(A);
% Currently this function just strips whitespace

A = regexprep(A,'\W+','');
% b = unique(A) ;
% for i = 1:length(b),
%     v = strmatch(b{i},A);
%     if length(v) > 1,
%         q = strcat(strvcat(A(v)),[' ';strjust(num2str([2:length(v)]'),'left')]);
%         q = regexprep(q,'[\s]+','');
%         A(v) = cellstr(q);
%     end
% end
colnames = A;
