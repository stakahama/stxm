%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~rspline.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function rS = rspline(x,y)

xx = linspace(nanmin(x),nanmax(x),3*length(x));
yy = spline(x,y,xx);
rS = struct('x',num2cell(xx),'y',num2cell(yy));
