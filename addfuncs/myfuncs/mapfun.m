%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~mapfun.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function newL = mapfun(f,L)
% mapfun(f,L)

if length(L)==0,
    newL = {};
else
    newL = [{feval(f,L{1})},mapfun(f,L(2:end))];
end
