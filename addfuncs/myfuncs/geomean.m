%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~geomean.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function y = geomean(x)

y = repmat(NaN,1,size(x,2));
for j=1:size(x,2)
    f = x(find(~isnan(x(:,j))),j);
    if length(f) < 1, continue; end
    if prod(f) < 1e-10, y(j)=0; continue; end
    y(j) = prod(f)^(1/length(f));
end
