%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~colors.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function col = colors(s);

% s is string or cell array
try 
    colArray;
catch
    load 'c:\Documents and Settings\takahama\My Documents\UCSD\matlab\functions\myfuncs\colArray.mat';
end

if nargin < 1,
    g = colArray{1};
    disp(sort(g));
    return;
end

if iscell(s)
    col = zeros(length(s),3);
    for i=1:length(s)
        x = strmatch(s(i),colArray{1});
        x = x(1);
        col(i,:) = colArray{2}(x,:);
    end
elseif isnumeric(s) & s==1;
    disp(sort(colArray{1}));    
else
    x = strmatch(s,colArray{1});
    x = x(1);
    col = colArray{2}(x,:);
end
    
clear colArray;

