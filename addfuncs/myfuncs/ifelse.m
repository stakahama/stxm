%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~ifelse.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function retval = ifelse(logicVec,yes,no)
% ifelse(logicVec,yes,no)

logicVec = logical(logicVec);
if iscell(yes) | iscell(no),
    retval = cell(size(logicVec));
else
    retval = repmat(NaN,size(logicVec));
end

if prod(size(yes))==1,
  yes = repmat(yes,size(logicVec));
end
if prod(size(no))==1,
  no = repmat(no,size(logicVec));
end

retval(logicVec) = yes(logicVec);
retval(~logicVec) = no(~logicVec);

return
