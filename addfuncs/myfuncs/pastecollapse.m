%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~pastecollapse.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function stringval = pastecollapse(strcell,collapse)

s = reshape(cat(1,reshape(strcell,1,[]),...
  [repmat({collapse},1,length(strcell)-1),{''}]),...
  1,[]);
stringval = strcat(s{:});

return
