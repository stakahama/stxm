%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~getElem.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function retval = getElem(obj,fun,i)
% function retval = getElem(obj,fun,i)

if ~isequal('cell',class(obj)),
    obj = {obj};
end
[c{1:i}] = feval(fun,obj{:});
retval = c{i};

return
