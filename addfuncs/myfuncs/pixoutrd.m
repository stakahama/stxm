%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~pixoutrd.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function A = pixoutrd(filenm);

fid = fopen(filenm,'r');
headr = fscanf(fid,'%s',1);
outputname = fscanf(fid,'%s',1);
headr = fscanf(fid,'%s',4);
pixSE = fscanf(fid,'%d',4);
headr = fscanf(fid,'%s',2);
xyrg = fscanf(fid,'%f',2);
fclose(fid);

XValues = linspace(0,xyrg(1),pixSE(2)-pixSE(1)+1);
YValues = linspace(0,xyrg(2),pixSE(4)-pixSE(3)+1);
A = {XValues',YValues'};
