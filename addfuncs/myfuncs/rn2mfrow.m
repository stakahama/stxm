%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~rn2mfrow.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function [nx,ny] = rn2mfrow(nr) 

if nr <= 3 
  vec = [nr, 1];
elseif nr <= 6 
  vec = [floor((nr+1)/2), 2];
elseif nr <= 12 
  vec = [floor((nr+2)/3), 3];
else 
  nrow = ceil(sqrt(nr));
  ncol = ceil(nr/nrow);
  vec = [nrow,ncol];
end

nx = min(vec);
ny = max(vec);

return
