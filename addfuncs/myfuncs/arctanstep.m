%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~arctanstep.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function a = arctanstep(x,pos,wid)
% 
%x = linspace(280,305,1000);
%pos = 290;
%wid = 1;
xp = (x-pos)*0.991/(wid/2);
a = atan(xp);
