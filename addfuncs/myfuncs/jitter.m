%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~jitter.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function y = jitter(x,factor,amount)
% jitter(x,factor,amount)

if length(x) < 2;
    y = x;
    return;
end
d = min(diff(x(find(~isnan(x)))));
if d==0; d=1; end;
if nargin < 3; amount = d/5; end;
if nargin < 2; factor = 1; end;
if amount == 0; amount = d/50; end;
y = x+randn(length(x),1)*factor*amount/3;
