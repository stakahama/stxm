%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~mapfuni.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function newL = mapfuni(f,L)
% mapping function, iteratively implemented
% mapfuni(f,L)

newL = cell(size(L));
for i=1:length(L),
    newL{i} = feval(f,L{i});    
end

return
