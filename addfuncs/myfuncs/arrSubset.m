%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~arrSubset.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function retval = arrSubset(arr,Ind,sel)
% retval = arrSubset(arr,Ind)
if nargin < 3,
if isa(arr,'cell') & length(Ind)==1,
  retval = arr{Ind};
else,
  retval = arr(Ind);
end
else,
  if ~isempty(Ind) & isempty(sel),
    retval = arr(Ind,:);
  elseif isempty(Ind) & ~isempty(sel),
    retval = arr(:,sel);
  elseif ~isempty(Ind) & ~isempty(sel),
    retval = arr(Ind,sel);  
  end
end
return
