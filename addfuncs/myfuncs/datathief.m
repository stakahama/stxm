%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~datathief.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function  A = datathief(file,xrg,yrg)
% inputs: file,xrg,yrg

B = imread(file);
image(B);
set(gca,'DataAspectR',[1 1 1]);
[x y] = ginput;
x0 = x(1); y0 = y(3);
Dx = x(2)-x(1);
Dy = y(4)-y(3);
x(1:4) = []; y(1:4) = [];
xx = xrg(1) + (x - x0) ./ Dx .* diff(xrg);
yy = yrg(1) + (y - y0) ./ Dy .* diff(yrg);
%A = struct('x',xx,'y',yy);
A = struct('x',num2cell(xx),'y',num2cell(yy));
