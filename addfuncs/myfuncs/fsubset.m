%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~fsubset.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

function varargout = fsubset(x,v)
% A = fsubset(x,v)
% x is function output (can be cell or struct)
% v is subsetting array or string

if iscell(x),
    if length(v)==0,
        A = x;
    elseif length(v)==1,
        A = x{v};
    elseif length(v) > 1,
        A = x(v);
    end
elseif isstruct(x),
    A = x.(v);
else,
    if isempty(v),
        A = x(:);
    else
        A = x(v);        
    end
end

if iscell(A),
    varargout = A;
else
    varargout = {A};
end
