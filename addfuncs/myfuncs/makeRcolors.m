%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~makeRcolors.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

load colArray
newnames = regexprep(colArray{1},'[ ]+','');
newnames = regexprep(newnames,'([^()/]+).*','$1','tokenize');
ncol = size(colArray{2},1);
colcell = {};
nms = {};
for i = 1:ncol
    m = strmatch(newnames{i},newnames);
    if any(m < i), continue; end
    colcell = [colcell,{colArray{2}(i,:)}];
    nms = [nms,newnames(i)];
end
RColors = cell2struct(colcell,nms,2);
save RColors RColors

