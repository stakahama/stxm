function P=findpeaksG(x,y,SlopeThreshold,AmpThreshold,smoothwidth,peakgroup)
% Graphic version of findpeaks. Shows fit around each peak.
% Function to locate the positive peaks in a noisy x-y data
% set.  Detects peaks by looking for downward zero-crossings
% in the third derivative that exceed a threshold.
% Determines the position, height, and width of
% each peak by fitting the log of top part of the peak with
% a parabola.  Returns list (P) containing peak number and
% position, height, and width of each peak. SlopeThreshold,
% AmpThreshold, and smoothwidth control sensitivity
% Higher values will neglect smaller features. Peakgroup
% is the number of points around the "top part" of the peak.
% T. C. O'Haver, 1995.  Last revised July, 2006
global pos
global amp
global wid
d1=tsmooth(deriv(y),smoothwidth);
d2=-tsmooth(deriv(d1),smoothwidth);
d3=tsmooth(deriv(d2),smoothwidth);
d=d3;
    % figure(2);plot(x,d);grid  % Display smoothed derivative in Figure 2
n=round(peakgroup/2+1);
clear P;
peak=1;
for j=smoothwidth:length(y)-smoothwidth,
   if sign(d(j)) > sign (d(j+1)), % Detects zero-crossing
     if d(j)-d(j+1) > SlopeThreshold, % if slope of derivative is large enough
        if y(j) > AmpThreshold.*max(y),  % if height of peak is larger than AmpThreshold
          for k=1:peakgroup, % Create sub-group of points near peak
             xx(k)=x(j+k-n+1);yy(k)=y(j+k-n+1);
          end
       figure(2);plot(x(j-2.*peakgroup:j+2.*peakgroup),y(j-2.*peakgroup:j+2.*peakgroup),'r.');pause(.04);hold on
       coef=polyfit(xx,log(abs(yy)),2);  % Fit parabola to log10 of sub-group
       plot(xx,exp(polyval(coef,xx)));pause(.04);
       c1=coef(3);c2=coef(2);c3=coef(1);
       PeakX=-c2/(2*c3);   % Compute peak position and height or fitted parabola
       PeakY=exp(c1-c3*(c2/(2*c3))^2);
       MeasuredWidth=2.35703/(sqrt(2)*sqrt(-1*c3));
       figure(1);text(PeakX,PeakY,num2str(peak)) 
       figure(2);text(PeakX,.9.*PeakY,['Peak #' num2str(peak) ]); 
       if peak <= length(amp),
         title(['Peak Height = ' num2str(PeakY) '    Peak Position =' num2str(PeakX) '    Peak Width =' num2str(MeasuredWidth)])
         xlabel(['Height error = ' num2str(100.*(amp(peak)-PeakY)/amp(peak)) '%   '  'Position error = ' num2str(100.*(pos(peak)-PeakX)/pos(peak)) '%    ' 'Width error = ' num2str(100.*(wid(peak)-MeasuredWidth)/wid(peak)) '%']);
       end
       hold off;
       pause 
       % Construct matrix P. One row for each peak 
       % detected, containing the peak number, peak 
       % position (x-value) and peak height (y-value).
       P(peak,:) = [round(peak) PeakX PeakY MeasuredWidth];
       peak=peak+1;
     end
    end
  end
end