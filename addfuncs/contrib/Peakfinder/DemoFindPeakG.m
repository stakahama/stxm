% Graphical demo of findpeaks function, showing fit of each peak.
% Generates a synthetic set of data containing 25 narrow peaks of random amplitude.
warning off MATLAB:polyfit:RepeatedPointsOrRescale
format compact
figure(1);clf
clear
global amp
global pos
global wid
increment=0.1;
x=[1:increment:400];
% For each simulated peak, enter the amplitude, position, and width below
amp=0.1+rand(1,25);  % Amplitudes of the peaks
pos=[20:15:390];   % Positions of the peaks
pos=pos+3.*randn(size(pos));  % Shift the peaks about randomly so that some of the overlap a bit 
wid=4.*ones(size(pos));   % Widths of the peaks
Noise=.01;
% A = matrix containing one of the unit-amplidude peak in each of its srow
A = zeros(length(pos),length(x));
for k=1:length(pos)
  A(k,:)=gaussian(x,pos(k),wid(k)); % Or you can use lorentzian function
end
z=amp*A;  % Multiplies each row by the corresponding amplitude and adds them up
y=z+Noise.*randn(size(z));
figure(1);plot(x,y,'r')  % Graph the signal in red
title('Press any key to move to next peak')
hold on
SlopeThreshold=0.00000007;
AmpThreshold=.03;
SmoothWidth=mean(wid)./increment;  % SmoothWidth should be roughly equal to the peak widths (in points)
PeakWidth=mean(wid)./increment; % PeakWidth should be roughly equal to the peak widths  (in points)
start=cputime;
P=findpeaksG(x,y,SlopeThreshold,AmpThreshold,SmoothWidth,PeakWidth);
PeaksPerSecond=length(P)/(cputime-start)
'    Peak #   Position    Height    Width'
P  % Display table of peaks
% (Optional) Display table of peak measurement errors
%'    Peak #    SNR      X Error%  Y Error%  WidthError%'
%[P(:,1) amp'./Noise 100.*(P(:,2)-pos')./pos' 100.*(P(:,3)-amp')./amp' 100.*(P(:,4)-wid')./wid']
hold off
warning on MATLAB:polyfit:RepeatedPointsOrRescale