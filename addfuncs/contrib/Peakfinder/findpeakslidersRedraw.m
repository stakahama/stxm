global x
global y
global SlopeThreshold 
global AmpThreshold  
global SmoothWidth
global PeakWidth
global P
axes(h);
plot(x,y,'r')  % Graph the signal in red
P=findpeaks(x,y,SlopeThreshold,AmpThreshold,SmoothWidth,PeakWidth);
title([num2str(length(P)) ' peaks detected'])
xlabel(['SlopeT = ' num2str(SlopeThreshold) '    AmpT = ' num2str(AmpThreshold) '    SmoothWidth = ' num2str(SmoothWidth) '    PeakWidth = ' num2str(PeakWidth) ])
text(P(:, 2),P(:, 3),num2str(P(:,1)))  % Number the peaks found on the graph
axis([x(1) x(length(x)) -.1.*max(y) max(y)]); % Update plot