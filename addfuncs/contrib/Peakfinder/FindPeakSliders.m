% Interactive findpeaks function for pre-defined data in x,y. 
% Load a typical data set into the vectors x,y, then run this 
% m-file and adjust the 4 sliders to determine what values of the 
% parameters give the most reliable peak detection. Peak number and 
% position, height, and width of each peak is returned in the matrix P. 
% The 4 parameters are:
% SlopeThreshold (SlopeT) - Slope of the smoothed third-derivative that is  
%   taken to indicate a peak. Larger values will neglect small features.
% AmpThreshold (AmpT) - Any peaks with height less than this value are ignored.
% SmoothWidth - Width of smooth functions applied to data before slope is
%   measured. Larger values will neglect small features. The best value is 
%   about equal to the half-width of the peaks.
% PeakWidth - The number of points around the "top part" of the (unsmoothed)
%   peak that are taken to determine the peak height, positions, and width.
%   The best value is about equal to the half-width of the peaks.
%
% Note: If the slider ranges are not appropriate for your data, change 
%       them in lines 53-56.

warning off MATLAB:polyfit:RepeatedPointsOrRescale
format compact
global x
global y
global SlopeThreshold 
global AmpThreshold  
global SmoothWidth
global PeakWidth
global P
close
figure(1)

% Simulate data set
increment=x(2)-x(1); 

% Graph the signal in red
h=figure(1);
plot(x,y,'r') 
h2=gca;axis([x(1) x(length(x)) -.1.*max(y) max(y)]);
title('Findpeaks Explorer.  Vary the sliders to see how the parameters effect peak finding performance')

% Initial values of variable parameters
SlopeThreshold=0.0000003;
AmpThreshold=.1;
SmoothWidth=20;  % SmoothWidth should be roughly equal to the peak widths (in points)
PeakWidth=20; % PeakWidth should be roughly equal to the peak widths (in points)

% Find and number the peaks on the graph
warning off MATLAB:polyfit:RepeatedPointsOrRescale
P=findpeaks(x,y,SlopeThreshold,AmpThreshold,SmoothWidth,PeakWidth);
xlabel(['SlopeT = ' num2str(SlopeThreshold) '    AmpT = ' num2str(AmpThreshold) '    SmoothWidth = ' num2str(SmoothWidth) '    PeakWidth = ' num2str(PeakWidth) ])
text(P(:, 2),P(:, 3),num2str(P(:,1)))  % Number the peaks found on the graph

% Maximum ranges of the sliders (change as needed)
SlopeMax=.000002;
AmpMax=.5;
SmoothWidthMax=100;
PeakWidthMax=100;

% Draw the sliders
rtslid(h,@findpeaksliders1,h2,1,'Scale',[0 SlopeMax],'Def',SlopeThreshold,'Back',[0.9 0.9 0.9],'Label','SlopeT','Position',[0.03 0.5 0.03 0.35]);
rtslid(h,@findpeaksliders3,h2,0,'Scale',[3 PeakWidthMax],'Def',PeakWidth,'Back',[0.9 0.9 0.9],'Label','PeakWidth','Position',[0.95 0.05 0.03 0.35]);
rtslid(h,@findpeaksliders2,h2,0,'Scale',[0 AmpMax],'Def',AmpThreshold,'Back',[0.9 0.9 0.9],'Label','AmpT','Position',[0.03 0.05 0.03 0.35]);
rtslid(h,@findpeaksliders4,h2,0,'Scale',[1 SmoothWidthMax],'Def',SmoothWidth,'Back',[0.9 0.9 0.9],'Label','SmoothWidth','Position',[0.95 0.5 0.03 0.35]);