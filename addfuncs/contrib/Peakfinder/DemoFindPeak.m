% Demos findpeaks functions on noisy synthetic data. You can adjust 
% the peak function in line 20 and the peak parameters in lines 12, 13,  
% and 14 to create a simulated signal that is similar to your real data,  
% then try different values of the function arguments (in lines 29-32) 
% to determine what values give the most reliable peak detection.
warning off MATLAB:polyfit:RepeatedPointsOrRescale
format compact
figure(1);clf
clear
increment=0.1;
x=[1:increment:400];
% For each simulated peak, enter the amplitude, position, and width below
amp=randn(1,75);  % Amplitudes of the peaks
pos=[10:5:380];   % Positions of the peaks
wid=2.*ones(size(pos));   % Widths of the peaks
Noise=.01;
% A = matrix containing one of the unit-amplidude peak in each of its srow
A = zeros(length(pos),length(x));
for k=1:length(pos)
  if amp(k)>0, A(k,:)=gaussian(x,pos(k),wid(k)); end; % Or you can use any other peak function
end
z=amp*A;  % Multiplies each row by the corresponding amplitude and adds them up
y=z+Noise.*randn(size(z));
figure(1);plot(x,y,'r')  % Graph the signal in red
title('Detected peaks are numbered. Peak table is printed in Command Window')
SlopeThreshold=0.0000005;
AmpThreshold=.01;
SmoothWidth=mean(wid)./increment;  % SmoothWidth should be roughly equal to the peak widths (in points)
PeakWidth=mean(wid)./increment; % PeakWidth should be roughly equal to the peak widths (in points)
start=cputime;
P=findpeaks(x,y,SlopeThreshold,AmpThreshold,SmoothWidth,PeakWidth);
PeaksPerSecond=length(P)/(cputime-start)
'    Peak #   Position    Height    Width'
P  % Display table of peaks
figure(1);text(P(:, 2),P(:, 3),num2str(P(:,1)))  % Number the peaks found on the graph
warning on MATLAB:polyfit:RepeatedPointsOrRescale