% Interactive findpeaks function on noisy synthetic data. You can adjust 
% the peak function in line 39 and the parameters in lines 32-35  
% to create a simulated signal that is similar to your real data,  
% then run this m-file and adjust the 4 sliders to determine what
% values of the parameters give the most reliable peak detection. 
% The 4 parameters are:
% SlopeThreshold - Slope of the smoothed third-derivative that is taken 
%  to indicate a peak. Larger values will neglect small features.
% AmpThreshold - Any peaks with height less than AmpThreshold are ignored.
% SmoothWidth - Width of smooth functions applied to data before slope is
%   measured. Larger values will neglect small features. The best value is 
%   about equal to the half-width of the peaks.
% PeakWidth - The number of points around the "top part" of the (unsmoothed)
%  peak that are taken to determine the peak height, positions, and width.
%  The best value is about equal to the half-width of the peaks.
warning off MATLAB:polyfit:RepeatedPointsOrRescale
format compact
global x
global y
global SlopeThreshold 
global AmpThreshold  
global SmoothWidth
global PeakWidth
global P
figure(1);clf

% Simulate data set
increment=0.1;
x=[1:increment:400];
% For each simulated peak, enter the amplitude, position, and width below
amp=randn(1,75);  % Amplitudes of the peaks
pos=[10:5:380];   % Positions of the peaks
wid=2.*ones(size(pos));   % Widths of the peaks
Noise=.01;
% A = matrix containing one of the unit-amplidude peak in each of its srow
A = zeros(length(pos),length(x));
for k=1:length(pos)
  if amp(k)>0, A(k,:)=gaussian(x,pos(k),wid(k)); end; % Or you can use any other peak function
end
z=amp*A;  % Multiplies each row by the corresponding amplitude and adds them up
y=z+Noise.*randn(size(z));

% Graph the signal in red
h=figure(1);
plot(x,y,'r') 
h2=gca;axis([x(1) x(length(x)) -.1.*max(y) max(y)]);
title('Findpeaks Explorer.  Vary the sliders to see how the parameters effect peak finding performance')

% Initial values of variable parameters
SlopeThreshold=0.0000003;
AmpThreshold=.1;
SmoothWidth=mean(wid)./increment;  % SmoothWidth should be roughly equal to the peak widths (in points)
PeakWidth=mean(wid)./increment; % PeakWidth should be roughly equal to the peak widths (in points)

% Find and number the peaks on the graph
warning off MATLAB:polyfit:RepeatedPointsOrRescale
P=findpeaks(x,y,SlopeThreshold,AmpThreshold,SmoothWidth,PeakWidth);
xlabel(['SlopeT = ' num2str(SlopeThreshold) '    AmpT = ' num2str(AmpThreshold) '    SmoothWidth = ' num2str(SmoothWidth) '    PeakWidth = ' num2str(PeakWidth) ])
text(P(:, 2),P(:, 3),num2str(P(:,1)))  % Number the peaks found on the graph

% Maximum ranges of the sliders (change as needed)
SlopeMax=.000002;
AmpMax=.5;
SmoothWidthMax=100;
PeakWidthMax=100;

% Draw the sliders
rtslid(h,@findpeaksliders1,h2,1,'Scale',[0 SlopeMax],'Def',SlopeThreshold,'Back',[0.9 0.9 0.9],'Label','SlopeT','Position',[0.03 0.5 0.03 0.35]);
rtslid(h,@findpeaksliders3,h2,0,'Scale',[3 PeakWidthMax],'Def',PeakWidth,'Back',[0.9 0.9 0.9],'Label','PeakWidth','Position',[0.95 0.05 0.03 0.35]);
rtslid(h,@findpeaksliders2,h2,0,'Scale',[0 AmpMax],'Def',AmpThreshold,'Back',[0.9 0.9 0.9],'Label','AmpT','Position',[0.03 0.05 0.03 0.35]);
rtslid(h,@findpeaksliders4,h2,0,'Scale',[1 SmoothWidthMax],'Def',SmoothWidth,'Back',[0.9 0.9 0.9],'Label','SmoothWidth','Position',[0.95 0.5 0.03 0.35]);