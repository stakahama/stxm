function CLim = newclim(BeginSlot,EndSlot,CDmin,CDmax,CmLength)
%                Convert slot number and range
%                to percent of colormap
% http://www.mathworks.com/access/helpdesk/help/techdoc/creating_plots/index.html?/access/helpdesk/help/techdoc/creating_plots/f1-12057.html&http://www.google.com/search?q=matlab+help+clim&ie=utf-8&oe=utf-8&rls=org.mozilla:en-US:official&client=firefox-a
   PBeginSlot    = (BeginSlot - 1) / (CmLength - 1);
   PEndSlot      = (EndSlot - 1) / (CmLength - 1);
   PCmRange      = PEndSlot - PBeginSlot;
   % 				Determine range and min and max 
   % 				of new CLim values
   DataRange     = CDmax - CDmin;
   ClimRange     = DataRange / PCmRange;
   NewCmin       = CDmin - (PBeginSlot * ClimRange);
   NewCmax       = CDmax + (1 - PEndSlot) * ClimRange;
   CLim          = [NewCmin,NewCmax];
end
