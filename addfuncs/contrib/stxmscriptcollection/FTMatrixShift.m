function B=FTMatrixShift(A,dy,dx)
%function B=FTMatrixShift(A,dy,dx)
%
%Function used to shift the content of a grayscale image 
%by non-integer pixels values. The non-integer pixel shifts
%are performed in Fourier space by applying a linear phase
%to the Fourier transfrom data
%T.R. Henn February 2009
%
% Inputs
% ------
% A           grayscale image matrix
% dx          pixel shift value in horizontal direction
% dy          pixel shift in vertical direction
% 
% 
% Output
% B           shifted grayscale image matrix

[Ny,Nx]=size(A);
rx = floor(Nx/2)+1; 
fx = ((1:Nx)-rx)/(Nx/2);
ry = floor(Ny/2)+1; 
fy = ((1:Ny)-ry)/(Ny/2);
px = ifftshift(exp(-1i*dx*pi*fx))';
py = ifftshift(exp(-1i*dy*pi*fy))'; 

[yphase,xphase]=meshgrid(py,px);

yphase=yphase.';
xphase=rot90(xphase);

B=abs(ifft2(fft2(A).*yphase.*xphase));

return