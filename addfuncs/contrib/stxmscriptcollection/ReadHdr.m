function [eVenergy,Xvalue,Yvalue]=ReadHdr(file)
%function [evenergy,Xvalue,Yvalue]=ReadHdr(file)
%
%Reads STXM data header file (.hdr) to extract photon energies and x and y image lengths 
%R.C. Moffet, T.R. Henn February 2009
%
%Inputs
%------
%file           .hdr header file path
%
%Outputs
%-------
%eVenergy       vector containing photon energies used to record STXM images
%Xvalue         length of horizontal STXM image axis in �m
%Yvalue         length of vertical STXM image axis in �m





filestream = fopen(file, 'r');    
cnt=1;
while feof(filestream) == 0
    line=fgets(filestream);
    energypos=findstr(line,'StackAxis');
    pos3=findstr(line,'; XRange =');
    
    if ~isempty(energypos)
        line=fgets(filestream);
        pos1=findstr(line,'(');
        pos2=findstr(line,')');
        energyvec=str2num(line(pos1+1:pos2-1));
        eVenergy=energyvec(2:end)';
    elseif ~isempty(pos3)
        pos4=findstr(line,'; YRange =');
        pos5=findstr(line,'; XStep =');
        Xvalue=str2num(line(pos3+10:pos4-1));
        Yvalue=str2num(line(pos4+10:pos5-1));
    end
    
    clear line pos3;
    cnt=cnt+1;
end
fclose(filestream);
