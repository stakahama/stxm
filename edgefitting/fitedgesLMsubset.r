options(warn=-1,stringsAsFactors=FALSE)

## functions
gamma <- function(D,R) {
  val <- vector("numeric",length(D))
  function(Rstar) {
    index <- ((1-D/R)/Rstar <= 1)
    replace(val,index,
            Rstar*sin(acos(Rstar^-1*(1-D[index]/R)))/
            sin(acos(1-D[index]/R)))
  }
}
fitedges <- function(D,alpha,bw,R) {
  g <- gamma(D,R)
  function(Rstar) {
    x <- g(Rstar)
    lm(alpha ~ bulk + surf -1,
       data=data.frame(alpha=alpha,bulk=x,surf=1-x),
       subset=bw & D < .5*R,na.action=na.exclude)
  }
}
avefit <- function(D,alpha,bw)
  lm(alpha~1,subset=bw,na.action=na.exclude)
loadfile <- function(filename) {
  L <- list()
  f <- file(filename)
  open(f)
  L$R <- scan(f,n=1,quiet=TRUE)
  L$Edge <- read.table(f,sep="\t",na.string="NaN",
                       colClasses=c("numeric","numeric","integer"),
                       col.names=c("D","alpha","bw"))
  L$Edge$bw <- as.logical(L$Edge$bw)
  close(f)
  return(L)
}
getfitobj <- function(filename,grid,con,outputfile) {
  L <- loadfile(filename)
  pname <- sub("\\_edge\\.txt","",filename)
  ##print(pname)
  dolmfit <- do.call(fitedges,c(L[['Edge']],L['R']))
  lmout <- lapply(grid,dolmfit)
  aicvals <- sapply(lmout,function(x)
                    if( any(is.na(coef(x))) ) NA else
                    AIC(x,k=length(fitted(x))))
  index <- which.min(aicvals)
  preferred <- lmout[[index]]
  rstar <- grid[index]
  avemodel <- do.call(avefit,L[['Edge']])  
  output <- c(R=L$R,
              Rstar=rstar,
              coef(preferred),
              BIClayered=aicvals[index],
              BICmean=AIC(avemodel,k=length(fitted(avemodel))))
  write2file(con,pname,output)
  makeplot(pname,L$Edge,output,avemodel,preferred,outputfile)
  output
}
#write2file
write2file <- function(con,pname,output)
  cat(sprintf("%s\t%s",pname,
              paste(sprintf("%.03f",output),collapse="\t")),"\n",
      file=con)
makeplot <- function(pname,edge,parms,avemodel,preferred,outputfile) {
  Dlin <-
    do.call(seq,as.list(c(range(with(edge,D[bw]),na.rm=TRUE),length=200)))
  g <- gamma(Dlin,parms["R"])(parms["Rstar"])
  ##
  png(outputfile)
  with(subset(edge,bw),
       plot(D,alpha,pch=3,col=8,
            xlab="D",ylab=expression(alpha),
            main=pname))
  lines(cbind(Dlin,coef(avemodel)),col=2)  
  lines(Dlin,
        predict(preferred,data.frame(bulk=g,surf=1-g)),
        col=4)
  winner <- replace(rep(1,6),(if(parms[5] < parms[6]) 5 else 6),3)
  for( i in 1:length(parms) )
    text(par("usr")[2],par("usr")[4]-par("cxy")[2]*(i-1),
         substitute(par == val,
                    list(par=names(parms)[i],val=round(parms[i],2))),
         col=winner[i],adj=c(1,1))
  dev.off()
  NULL
}

grid <- cos(seq(0,pi/2,,20))
## (function(x) plot(cos(x),jitter(rep(1,length(x))),type='p',
##                   ylim=c(0,2)))(grid)

## application
trygetfitobj <- function(x,g,con,d)
  tryCatch(getfitobj(x,g,con,d),error=function(e) NULL)

parms <- tail(commandArgs(),2)

con <- ""
fitpars <- trygetfitobj(parms[1],grid,con,parms[2])
