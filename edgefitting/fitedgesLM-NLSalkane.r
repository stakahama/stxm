source("NLSmodel.r")
fitedgesLM <- function(D,alpha,bw,R) {
  g <- gamma(D,R)
  function(Rstar) {
    x <- g(Rstar)
    lm(alpha ~ bulk + surf -1,
       data=data.frame(alpha=alpha,bulk=x,surf=1-x),
       subset=bw,na.action=na.exclude)
  }
}
fitedgesNLS <- function(D,alpha,bw,R) {
  s <- bw & is.finite(alpha) & is.finite(D) & D < R
  alpha <- alpha[s]; D <- D[s]
  g <- gamma(D,R)
  function(initparms)
    nls(alpha ~ g(Rstar)*alpha_bulk+(1-g(Rstar))*alpha_surf,
        data=list(alpha=alpha,D=D),
        algorithm="port",
        start=c(Rstar=initparms[1],
          alpha_bulk=initparms[2],
          alpha_surf=initparms[3]),
        lower=c(Rstar=0,
          alpha_bulk=0,
          alpha_surf=0),
        upper=c(Rstar=1,
          alpha_bulk=1e5,
          alpha_surf=1e5))
}
avefit <- function(D,alpha,bw)
  lm(alpha~1,subset=bw,na.action=na.exclude)
BIC <- function(x) AIC(x,k=log(length(fitted(x))))
getfitobj <- function(filename) {
  cat(sub("\\.txt","",filename),":\t")
  L <- loadfile(filename)
  dat <- with(L,list(D=Edge$D,alpha=Edge$alpha,bw=Edge$bw,R=R))
  ##
  rmin <- (function(x) function(f) acos(x + (1-x)*f)
           )(with(dat,1-max(D,na.rm=TRUE)/R))
  rstar <-
    tryCatch(cos(seq(1e-2,rmin(0),,20)),error=function(e) NULL)
  if( is.null(rstar) ) return(NULL)
  ##
  lmmodel <- do.call(fitedgesLM,dat)
  lmfits <- lapply(rstar,lmmodel)
  lmbic <- sapply(lmfits,function(x) if( is.null(x) ) NA else BIC(x))
  valid <- apply(!is.na(sapply(lmfits,coef)),2,all)  
  lmindex <- seq(along=lmfits)[valid][which.min(lmbic[valid])]
  cat(round(coef(lmfits[[lmindex]]),2),",")
  ##
  grid <- (function(x)
           `dimnames<-`(as.matrix(expand.grid(rmin(seq(1e-3,0.999,,3)),
                                              seq(0.1*x,1.5*x,,3),
                                              seq(0.5*x,5*x,,3))),NULL)
           )(max(dat$alpha,na.rm=TRUE))
  grid <- rbind(c(rstar[lmindex],unname(coef(lmfits[[lmindex]]))),grid)
  nlsmodel <- do.call(fitedgesNLS,dat)
  nlsfits <- apply(grid,1,function(p)
                   tryCatch(nlsmodel(p),error=function(e) NULL))
  nlsbic <- sapply(nlsfits,function(x) if(is.null(x)) NA else BIC(x))
  nlsindex <- which.min(nlsbic)
  ##
  if( length(nlsindex)==0 ) nlsindex <- NA
  try(cat(nlsindex, round(coef(nlsfits[[nlsindex]]),2)),TRUE)
  cat(",")
  ##
  avefit <- do.call(avefit,L$Edge)   
  avebic <- BIC(avefit)
  cat(avebic,"\n")

  list(lm=c(rstar=rstar[lmindex],
         coef(lmfits[[lmindex]]),
         bic=lmbic[lmindex],
         dev=deviance(lmfits[[lmindex]])),
       nls=c(coef(nlsfits[[nlsindex]]),
         bic=unname(nlsbic)[nlsindex],
         dev=deviance(nlsfits[[nlsindex]]),
         index=nlsindex),
       ave=c(coef(avefit),bic=avebic,dev=deviance(avefit)))
}
loadfile <- function(filename) {
  L <- list()
  f <- file(filename)
  open(f)
  L$R <- scan(f,n=1,quiet=TRUE)
  L$Edge <- read.table(f,sep="\t",na.string="NaN",
                       colClasses=c("numeric","numeric","integer","numeric"),
                       col.names=c("D","alpha","bw","alkane"))
  L$Edge$bw <- as.logical(L$Edge$bw)
  L$Edge$alpha <- L$Edge$alkane
  L$Edge$alkane <- NULL
  close(f)
  return(L)
}

## application
surfaceCOOH <- readLines("../pexport/list_surfaceCOOH.txt")
fi <- list.files(".","edge\\.txt")
fitpars <- lapply(`names<-`(fi[pmatch(surfaceCOOH, fi)],surfaceCOOH),
                  getfitobj)
save(fitpars,file="LM-NLSfitpars-alkane.rda")

## load("NLSfitpars.rda")
## debug(getfitobj)
## lapply(fi[pmatch(surfaceCOOH, fi)][1],getfitobj,grid)
