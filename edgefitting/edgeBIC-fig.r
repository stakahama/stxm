options(stringsAsFactors=FALSE)
library(RColorBrewer)
library(reshape)

load("../../figuresforpaper/mydat-merged.rda")
load("LM-NLSfitpars_revised.rda")
testNLS <- sapply(fitpars,function(p)
       tryCatch((function(x) if(length(x)==0) FALSE else x
                 )(p$nls[["bic"]] < p$ave[["bic"]]),
                error=function(e) FALSE))
testLM <- sapply(fitpars,function(p)
       tryCatch((function(x) if(length(x)==0) FALSE else x
                 )(p$lm[["bic"]] < p$ave[["bic"]]),
                error=function(e) FALSE))

length(which(testNLS))
length(which(testLM))

ffp <- Filter(function(x) length(x) > 0,fitpars)
null2na <- function(x,i)
  if(is.null(unlist(x[i]))) rep(NA,length(i)) else unlist(x[i])
l2m <- function(m)
  tryCatch(`mode<-`(m,"numeric"),error=function(e)
           matrix(NA,ncol=2,nrow=3,dimnames=dimnames(m)))
bicdev <-
  (function(fp)
   Map(function(x,v) {
     ((function(d) data.frame(particle=x,fit=rownames(d),
                              l2m(d),row.names=NULL))
      (t(sapply(v,null2na,c("bic","dev")))))
   },names(fp),fp))(ffp)
bicdev <- do.call(rbind,unname(bicdev))

safemin <- function(x)
  ((function(v) if( length(v) == 0 ) 1 else v)
    (which.min(x)))

best <- local({
  ##
  m <- melt(bicdev,measure.vars=c("bic","dev"),variable_name="metric")
  ca <- cast(m,particle + metric ~ fit)
  sapply(split(ca,ca$metric),function(x){
         `names<-`(names(x)[3:5][apply(as.matrix(x[3:5]),1,safemin)],
                   x$particle)})
})
dev <- ifelse(best[,"bic"] == "ave", NA, best[,"dev"])

relabel <- function(x)
  do.call(paste,c(do.call(expand.grid,dimnames(x))[2:1],list(sep="_")))

nlspars <-
  `colnames<-`((function(m,n) m[match(n,rownames(m)),])
               ((function(x)
                 do.call(rbind,replace(x,sapply(x,is.null),list(rep(NA,6)))))
                (lapply(ffp,function(x) c(x$nls[[1]]))),
                names(ffp)),
               relabel(ffp[[1]]$nls[[1]]))

lmpars <-
  `colnames<-`((function(m,n) m[match(n,rownames(m)),])
               (t(sapply(ffp,function(x) c(x$lm$rstar,NA,t(x$lm[[2]])))),
                names(ffp)),
               c("Rstar_Estimate","Rstar_Std. Error",
                 relabel(ffp[[1]]$lm[[2]])))
catch <- function(x) if( is.null(x) ) rep(NA,6) else x
finalfits <-
  t(mapply(function(x,v,lst)catch(lst[[v]][x,]),
         names(dev),dev,MoreArgs=list(lst=list(lm=lmpars,nls=nlspars))))

fi <- (function(x) `names<-`(x,sub("\\_edge\\.txt","",x))
       )(list.files(,"\\edge\\.txt"))
Rp <- sapply(fi[pmatch(rownames(finalfits),fi)],function(x)
             scan(x,n=1,quiet=TRUE))

## library(lattice)
## spsymb <- trellis.par.get("superpose.symbol")

unc <- read.delim("../../figuresforpaper/uncertainty_table_mod.txt",
                  row.names=1)

X <- merge(data.frame(particle=names(Rp),R=Rp),
           data.frame(finalfits),,1,0)
X <- merge(X,mydat[,c('particle','project','fshape','meta')],1)
X <- merge(X,unc,,1,0)
#head(rev(sort(2*Rp)))
var <- "meta"
spsymb <- list()
spsymb$col <- brewer.pal(nlevels(X[[var]]),"Dark2")

na2zero <- function(x) replace(x,is.na(x),0)
## === begin plot ===
postscript("surfaceCOOHthickness.eps",onefile=FALSE,width=6,height=6,
           horizontal=FALSE)
cex <- 1.2
setmargins(); par(pty="s")
plot.new()
plot.window(xlim=log10(c(.1,10)),ylim=c(0,.6))
cont <- local({
    volume <- function(r) 4/3*pi * r^3
    volfrac <- function(R,thickness)
        (volume(R) - volume(R-thickness)) / volume(R)
    coord <- list(R= 10^seq(par("usr")[1],par("usr")[2],,20),
                  thickness = seq(par("usr")[3],par("usr")[4],,20))
    z <- do.call(outer,c(unname(coord),list(volfrac)))
    c((function(d) replace(d,"x",list(d$x*2))
       )(`names<-`(coord,c("x","y"))),
      list(z=replace(z,z > 1 & z < 0,NA)))
})
with(cont,contour(log10(x),y,z,add=TRUE,col=grey(.5),method="edge",labcex=1.1,
                  levels=seq(.2,.8,.2)))
local({
      p <- par("usr")
        x <- seq(p[1],p[2],,150)
        with(subset(data.frame(x=x,y=(10^x)/2),y < p[4]),
                    polygon(c(x,rev(x)),c(y,rep(p[4],length(y))),
                                           density=10,col=grey(.5),
                            angle=45))
  })

with(X,{
  Rstar <- Rstar_Estimate
  impute <- function(x)
    replace(x,is.na(x) | x<.Machine$double.eps,
            mean(x[x>=.Machine$double.eps],na.rm=TRUE))
  Rstar.Err <- sqrt(R^2*na2zero(impute(Rstar_Std..Error))^2 +
                    (1-Rstar)^2*(diam.sd/2/sqrt(6))^2)
  arrows(log10(diam.min),(1-Rstar)*R,log10(diam.max),(1-Rstar)*R,
           col=spsymb$col[eval(parse(text=var))],
         length=.025,code=3,angle=90)
  arrows(log10(2*R),(1-Rstar)*R-Rstar.Err,log10(2*R),(1-Rstar)*R+Rstar.Err,
           col=spsymb$col[eval(parse(text=var))],
         length=.025,code=3,angle=90)
  points(log10(2*R),(1-Rstar)*R,col=spsymb$col[eval(parse(text=var))],
       pch=ifelse(fshape=="spherical",21,4),bg="white",
       cex=1.4,lwd=2)
})
title(xlab=expression("Particle size"~(mu*m)),
      ylab=expression("Coating thickness"~(mu*m)),cex.lab=cex)
(function(x) axis(1,at=log10(x),lab=x,cex.axis=cex))(c(.1,.2,.5,1,2,5,10))
axis(1,at=log10(outer(1:9,10^(-1:0))),lab=FALSE,tck=-0.01)
axis(2,cex.axis=cex)
legend("topleft",pch=13,col=spsymb$col[1:nlevels(X[[var]])],
       levels(X[[var]]),bg="white")
box()
dev.off()

((function(d,f) file.copy(f,file.path(d,f),overwrite=TRUE))
("/Users/stakahama/Documents/Work/UCSD/projects/Morphology/paper/2009-07_revision/figures",
 "surfaceCOOHthickness.eps"))

## write.table(X,"edgefitpars.txt",sep="\t",row=FALSE,quote=FALSE)
## range(with(X,(volume(R) - volume(R-(1-Rstar)*R)) / volume(R)),na.rm=TRUE)


###
