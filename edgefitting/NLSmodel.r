
## functions
coreshellmodel <- function(D,R)
  function(Rstar,alpha_bulk,alpha_surf) {
    g <- gamma(D,R)
    alpha <- g(Rstar)*alpha_bulk+(1-g(Rstar))*alpha_surf
    return(alpha)
  }
##flatten <- function(x) ifelse(x > 1,1,x)
gamma <- function(D,R) {
  val <- numeric(length(D))
  function(Rstar) {
    index <- ((1-D/R)/Rstar <= 1)
    replace(val,index,
            Rstar*sin(acos(Rstar^-1*(1-D[index]/R)))/
            sin(acos(1-D[index]/R)))
  }
}
loadfile <- function(filename) {
  L <- list()
  f <- file(filename)
  open(f)
  L$R <- scan(f,n=1,quiet=TRUE)
  L$Edge <- read.table(f,sep="\t",na.string="NaN",
                       colClasses=c("numeric","numeric","integer"),
                       col.names=c("D","alpha","bw"))
  L$Edge$bw <- as.logical(L$Edge$bw)  
  close(f)
  return(L)
}
