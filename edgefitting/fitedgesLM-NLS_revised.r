options(stringsAsFactors=FALSE)

## --- layered model ---
coreshellmodel <- function(D,R)
  function(Rstar,alpha_bulk,alpha_surf) {
    g <- gamma(D,R)
    alpha <- g(Rstar)*alpha_bulk+(1-g(Rstar))*alpha_surf
    return(alpha)
  }
gamma <- function(D,R) {
  val <- numeric(length(D))
  function(Rstar) {
    index <- ((1-D/R)/Rstar <= 1)
    replace(val,index,
            Rstar*sin(acos(Rstar^-1*(1-D[index]/R)))/
            sin(acos(1-D[index]/R)))
  }
}
##flatten <- function(x) ifelse(x > 1,1,x)

## --- I/O, fitting functions ---
loadfile <- function(f) {
  L <- list()
  L$R <- scan(f,n=1,quiet=TRUE)
  L$Edge <- 
    tryCatch(read.table(f,sep="\t",na.string="NaN",skip=1,
                        colClasses=c("numeric","numeric","integer"),
                        col.names=c("D","alpha","bw")),
             error=function(e)
             read.table(f,sep="\t",na.string="NaN",skip=1,
                        colClasses=c("numeric","numeric","integer",
                          "numeric"),
                        col.names=c("D","alpha","bw",
                          "unknown")))[,1:3]
  L$Edge$bw <- as.logical(L$Edge$bw)  
  return(L)
}
fitedgesLM <- function(D,alpha,bw,R) {
  g <- gamma(D,R)
  function(Rstar) {
    x <- g(Rstar)
    lm(alpha ~ bulk + surf -1,
       data=data.frame(alpha=alpha,bulk=x,surf=1-x),
       subset=bw,na.action=na.exclude)
  }
}
fitedgesNLS <- function(D,alpha,bw,R) {
  s <- bw & is.finite(alpha) & is.finite(D) & D < R
  alpha <- alpha[s]; D <- D[s]
  g <- gamma(D,R)
  function(initparms)
    nls(alpha ~ g(Rstar)*alpha_bulk+(1-g(Rstar))*alpha_surf,
        data=list(alpha=alpha,D=D),
        algorithm="port",
        start=c(Rstar=initparms[1],
          alpha_bulk=initparms[2],
          alpha_surf=initparms[3]),
        lower=c(Rstar=0,
          alpha_bulk=0,
          alpha_surf=0),
        upper=c(Rstar=1,
          alpha_bulk=1e5,
          alpha_surf=1e5))
}
avefit <- function(D,alpha,bw)
  lm(alpha~1,subset=bw,na.action=na.exclude)
BIC <- function(x) AIC(x,k=log(length(fitted(x))))
coeferr  <- function(x)
  tryCatch(t(summary(x)$coefficients[,1:2]),error=function(e)
           tryCatch(coef(x),error=function(e)
                    NA))
getfitobj <- function(filename) {
  cat(sub("\\.txt","",filename),":\t")
  L <- loadfile(filename)
  dat <- with(L,list(D=Edge$D,alpha=Edge$alpha,bw=Edge$bw,R=R))
  ##
  rmin <- (function(x) function(f) acos(x + (1-x)*f)
           )(with(dat,1-max(D,na.rm=TRUE)/R))
  rstar <-
    tryCatch(cos(seq(1e-2,rmin(0),,20)),error=function(e) NULL)
  if( is.null(rstar) ) return(NULL)
  ##
  lmmodel <- do.call(fitedgesLM,dat)
  lmfits <- lapply(rstar,lmmodel)
  lmbic <- sapply(lmfits,function(x) if( is.null(x) ) NA else BIC(x))
  valid <- apply(!is.na(sapply(lmfits,coef)),2,all)  
  lmindex <- seq(along=lmfits)[valid][which.min(lmbic[valid])]
  cat(round(coef(lmfits[[lmindex]]),2),",")
  ##
  grid <- (function(x)
           `dimnames<-`(as.matrix(expand.grid(rmin(seq(1e-3,0.999,,3)),
                                              seq(0.1*x,1.5*x,,3),
                                              seq(0.5*x,5*x,,3))),NULL)
           )(max(dat$alpha,na.rm=TRUE))
  grid <- rbind(c(rstar[lmindex],unname(coef(lmfits[[lmindex]]))),grid)
  nlsmodel <- do.call(fitedgesNLS,dat)
  nlsfits <- apply(grid,1,function(p)
                   tryCatch(nlsmodel(p),error=function(e) NULL))
  nlsbic <- sapply(nlsfits,function(x) if(is.null(x)) NA else BIC(x))
  nlsindex <- which.min(nlsbic)
  ##
  if( length(nlsindex)==0 ) nlsindex <- NA
  try(cat(nlsindex, round(coef(nlsfits[[nlsindex]]),2)),TRUE)
  cat(",")
  ##
  avefit <- do.call(avefit,L$Edge)   
  avebic <- BIC(avefit)
  cat(avebic,"\n")
  list(lm=list(rstar=rstar[lmindex],
         coeferr(lmfits[[lmindex]]),
         bic=lmbic[lmindex],
         dev=deviance(lmfits[[lmindex]])),
       nls=list(coeferr(nlsfits[[nlsindex]]),
         bic=unname(nlsbic)[nlsindex],
         dev=deviance(nlsfits[[nlsindex]]),
         index=nlsindex),
       ave=list(coeferr(avefit),bic=avebic,dev=deviance(avefit)))
}

## --- application ---

fi <- list.files(".","edge\\.txt")
fitpars <- lapply(`names<-`(fi[pmatch(surfaceCOOH, fi)],surfaceCOOH),
                  getfitobj)
save(fitpars,file="LM-NLSfitpars_revised.rda")

