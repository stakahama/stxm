
## functions
gamma <- function(D,R) {
  val <- vector("numeric",length(D))
  function(Rstar) {
    index <- ((1-D/R)/Rstar <= 1)
    replace(val,index,
            Rstar*sin(acos(Rstar^-1*(1-D[index]/R)))/
            sin(acos(1-D[index]/R)))
  }
}
fitedges <- function(D,alpha,bw,R) {
  g <- gamma(D,R)
  function(Rstar) {
    x <- g(Rstar)
    lm(alpha ~ bulk + surf -1,
       data=data.frame(alpha=alpha,bulk=x,surf=1-x),
       subset=bw,na.action=na.exclude)
  }
}
avefit <- function(D,alpha,bw)
  lm(alpha~1,subset=bw,na.action=na.exclude)
loadfile <- function(filename) {
  L <- list()
  f <- file(filename)
  open(f)
  L$R <- scan(f,n=1,quiet=TRUE)
  L$Edge <- read.table(f,sep="\t",na.string="NaN",
                       colClasses=c("numeric","numeric","integer"),
                       col.names=c("D","alpha","bw"))
  L$Edge$bw <- as.logical(L$Edge$bw)
  close(f)
  return(L)
}
getfitobj <- function(filename,grid,con,dir) {
  L <- loadfile(filename)
  pname <- sub("\\_edge\\.txt","",filename)
  print(pname)
  dolmfit <- do.call(fitedges,c(L[['Edge']],L['R']))
  lmout <- lapply(grid,dolmfit)
  aicvals <- sapply(lmout,function(x)
                    if( any(is.na(coef(x))) ) NA else
                    AIC(x,k=length(fitted(x))))
  index <- which.min(aicvals)
  preferred <- lmout[[index]]
  rstar <- grid[index]
  avemodel <- do.call(avefit,L[['Edge']])  
  output <- c(R=L$R,
              Rstar=rstar,
              coef(preferred),
              BICcoreshell=aicvals[index],
              BICmean=AIC(avemodel,k=length(fitted(avemodel))))
  write2file(con,pname,output)
  makeplot(pname,L$Edge,output,avemodel,preferred,dir)
}
#write2file
write2file <- function(con,pname,output)
  cat(sprintf("%s\t%s",pname,
              paste(sprintf("%.03f",output),collapse="\t")),"\n",
      file=con)
makeplot <- function(pname,edge,parms,avemodel,preferred,dir) {
  Dlin <-
    do.call(seq,as.list(c(range(with(edge,D[bw]),na.rm=TRUE),length=200)))
  g <- gamma(Dlin,parms["R"])(parms["Rstar"])
  ##
  png(file.path(dir,sprintf("%s_edge.png",pname)))
  with(subset(edge,bw),
       plot(D,alpha,pch=3,col=8,
            xlab="D",ylab=expression(alpha),
            main=pname))
  lines(cbind(Dlin,coef(avemodel)),col=2)  
  lines(Dlin,
        predict(preferred,data.frame(bulk=g,surf=1-g)),
        col=4)
  winner <- replace(rep(1,6),(if(parms[5] < parms[6]) 5 else 6),3)
  for( i in 1:length(parms) )
    text(par("usr")[2],par("usr")[4]-par("cxy")[2]*(i-1),
         substitute(par == val,
                    list(par=names(parms)[i],val=round(parms[i],2))),
         col=winner[i],adj=c(1,1))
  dev.off()
  NULL
}

grid <- cos(seq(0,pi/2,,20))
## (function(x) plot(cos(x),jitter(rep(1,length(x))),type='p',
##                   ylim=c(0,2)))(grid)

## application
trygetfitobj <- function(x,g,con,d)
  tryCatch(getfitobj(x,g,con,d),error=function(e) NULL)
fi <- list.files(".","edge\\.txt")
con <- file("edgeBIC.txt")
open(con,"w")
fitpars <- lapply(fi,trygetfitobj,grid,con,".")
close(con)


## getfitobj(fi[2],grid,con,".")
## debug(getfitobj)
## out <- getfitobj(fi[2],grid)
## aicvals <- sapply(out[[2]],function(x)
##            if( any(is.na(coef(x))) ) NA else
##            AIC(x,k=length(fitted(x))))
## avemodel <- do.call(meanfit,unname(out$data$Edge))
## c(aicvals[which.min(aicvals)],AIC(avemodel,k=length(fitted(avemodel)))
## preferred <- out[[2]][[which.min(aicvals)]]



## fits <- sapply(out[[2]],fitted)
## with(out$data,plot(Edge[,1],Edge[,2],pch=3,col=8))
## matlines(out$data$Edge[as.logical(out$data$Edge[,3]),1],fits)
## lines(out$data$Edge[as.logical(out$data$Edge[,3]),1],fits[,1],col=4,lwd=3)
## lines(out$data$Edge[as.logical(out$data$Edge[,3]),1],fits[,2],col=2,lwd=3)

## plot(out[[1]][[2]][1:2])
## out[[2]]


## out <- .Last.value

## debug(getfitobj)
## debug(dofit)

## save.image()

## initparms <- unlist(unname(grid[1,]))



## optim(c(1,1,1),function(x) abs(f(x) - observedvalues),...)

## R <- 0.5
## nls(alpha ~ g(D, Rstar) * alpha_bulk + (1 - g(D, Rstar)) * 
##     alpha_surf, data = data.frame(alpha, D), subset = as.logical(bw), 
##     algorithm = "port", na.action = na.omit,
##     start = list(Rstar = initparms[1], 
##       alpha_bulk = initparms[2], alpha_surf = initparms[3]), 
##     lower = list(Rstar = 0, alpha_bulk = 0, alpha_surf = 0), 
##     upper = list(Rstar = 1, alpha_bulk = 10, alpha_surf = 10))
## g(0.5,1)
## debug(g)
