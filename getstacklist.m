% get 'stacklist' from file 'stacksubset.m' and load variable 'stacks'
% which is structure of named MAT-files in the global space.

if exist('stacksubset.m','file'),
    disp('------------------------------------')
    disp('-----READING FROM stacksubset.m-----')
    disp('------------------------------------')    
    run stacksubset;    
    stacks = cell2struct(cellfun(@(x)strcat(x,'.mat'),stacklist,...
                                 'UniformOutput',0), 'name');
    clear stacklist;
else,
    stacks = dir(matdir);
end
