Code for processing STXM-NEXAFS stack images
===

Implemented in MATLAB; algorithm partially described in Takahama et al. (2008) and Takahama et al. (2010). Currently included are functions for processing carbon edge stacks (stxmfuncs/loadCfunctions.m).

More information is included in https://bitbucket.org/stakahama/stxm/src/raw/documents/description.pdf

S. Takahama, S. Gilardoni, and L. M. Russell. Single-particle oxidation state and morphology of atmospheric iron aerosols. Journal of Geophysical Research-Atmospheres, 113:D22202, November 2008.

S. Takahama, S. Liu, and L. M. Russell. Coatings and clusters of carboxylic acids in carbon-containing atmospheric particles from spectromicroscopy and their implications for cloud-nucleating and optical properties. Journal of Geophysical Research-Atmospheres, 115:D01202, January 2010.

