%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~export_aspectratio.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

% $$$ exports aspectratios in newaspectpars.txt

clear
run headr;
run userinputs;
run getstacklist; % creates variable 'stacks' in global space
stacks = matfiles; clear stacks;

resh = inline('reshape(x,[],1)');
fid = fopen(fullfile(outdir,'aspectpars.txt'),fperm);

count = 0;
for i=1:length(matfiles),
    if strcmp(matfiles(i).name(1),'.')~=0, continue; end
    disp(sprintf('%d: %s',i,matfiles(i).name));
    load(fullfile(matdir,matfiles(i).name));
    count = count + 1;
    if count == 1,
        fprintf(fid,'Particle\tadim\tbdim\ttouchbound\n');
    end
    for j=1:length(S.particles),
        fprintf(fid,'%s\t',S.particles(j).name);              
        asp = getaspects(addfds(S.particles(j),S,{'energy','xcoord', ...
                            'ycoord'}),'bw288',1,'ellipse');
        tbd = sum([resh(S.particles(j).bw288([1,end],:));...
                   resh(S.particles(j).bw288(:,[1,end]))]);
        fprintf(fid,'%.05f\t%.05f\t%d\n',asp(1:2),tbd);
    end
end
fclose(fid);
