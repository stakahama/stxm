
run headr;
sh = shapemetrics;

load('matfiles/10813052.mat')

refen = [285.6,288.85];
clim = [.02,.08; 0 .4];
el = @(x) reshape(x,[],1);

close all;

figure
% $$$ m = 64;
% $$$ colormap([1,1,1;jet(m)]);
for j=1:length(refen),
    [m,i] = min(abs(S.energy-refen(j)));
    subplot(2,2,j)
    imh = imagesc(S.xcoord,S.ycoord,S.stackarr(:,:,i));
    cb = colorbar;
    set(gca,'ydir','normal');
    set(gca,'dataaspectratio',[1,1,1]);
    set(gca,'clim',clim(j,:));
% $$$     Z = S.stackarr(:,:,i);
% $$$     cmin = min(Z(:));
% $$$     cmax = max(Z(:));
% $$$     C1 = min(m,round((m-1)*(Z-cmin)/(cmax-cmin))+1);
% $$$     set(imh,'CData',C1);
    xlabel('X position (\mum)') 
    ylabel('Y position (\mum)')     
    title(sprintf('Absolute Abs. at %.02f',refen(j)));
    subplot(2,2,j+2)
    normd = S.stackarr(:,:,i)./S.totalc;
    normd(~S.particles(1).bw) = NaN;
    imagesc(S.xcoord,S.ycoord,normd);
    cb = colorbar;
    set(gca,'ydir','normal');
    set(gca,'dataaspectratio',[1,1,1]);
% $$$     set(gca,'clim',clim(j,:))
    xlabel('X position (\mum)') 
    ylabel('Y position (\mum)')     
    title(sprintf('Normalized Abs. at %.02f',refen(j)));
end

figure
dist = feval(sh.distfun,S.xcoord,S.ycoord,S.particles(1).bw);
dist = dist-nanmin(dist(:));
hsvm = hsv;
cminval = -2*pi/size(hsvm,1);
colormap([1,1,1;hsvm]);
for j=1:length(refen),
    [m,i] = min(abs(S.energy-refen(j)));    
    theta = hsvangle(addfds(S,S.particles(1),{'bw'}),refen(j),'bw');
    bwtheta = theta; bwtheta(~S.particles(1).bw) = cminval;
    subplot(3,2,j)    
    imagesc(S.xcoord,S.ycoord,bwtheta)
    set(gca,'ydir','normal');
    set(gca,'dataaspectratio',[1,1,1]);
    xlabel('X position (\mum)') 
    ylabel('Y position (\mum)')     
    title(sprintf('Im. of %.02f',refen(j)));
    subplot(3,2,j+2)
    scatter(el(dist),el(S.stackarr(:,:,i)),6,el(theta));
    set(gca,'xdir','reverse','box','on')
    xlabel('Distance from surface (\mum)') 
    ylabel('Absolute Abs.')
    subplot(3,2,j+4)
    scatter(el(dist),el(S.stackarr(:,:,i))./el(S.totalc),6,el(theta));
    set(gca,'xdir','reverse','box','on')
    xlabel('Distance from surface (\mum)') 
    ylabel('Normalized Abs.')
end

figure
hsvm = hsv;
cminval = -2*pi/size(hsvm,1);
colormap([1,1,1;hsvm]);
for j=1:length(refen),
    [m,i] = min(abs(S.energy-refen(j)));    
    dist = radialdistfun(S,refen(j),S.particles(1).bw);
    dist = reshape(dist{1},size(S.particles(1).bw));
    dist(~S.particles(1).bw) = NaN;
    theta = hsvangle(addfds(S,S.particles(1),{'bw'}),refen(j),'bw');
    bwtheta = theta; bwtheta(~S.particles(1).bw) = cminval;
    subplot(3,2,j)    
    imagesc(S.xcoord,S.ycoord,bwtheta)
    set(gca,'ydir','normal');
    set(gca,'dataaspectratio',[1,1,1]);
    xlabel('X position (\mum)') 
    ylabel('Y position (\mum)')     
    title(sprintf('Im. of %.02f',refen(j)));
    subplot(3,2,j+2)
    scatter(el(dist),el(S.stackarr(:,:,i)),6,el(theta));
    set(gca,'box','on')
    xlabel('Radial distance (\mum)') 
    ylabel('Absolute Abs.')
    subplot(3,2,j+4)
    scatter(el(dist),el(S.stackarr(:,:,i))./el(S.totalc),6,el(theta));
    set(gca,'box','on')
    xlabel('Radial distance (\mum)') 
    ylabel('Normalized Abs.')
end
