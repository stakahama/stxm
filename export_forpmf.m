
run userinputs;

exportpath = '~/VirtualBoxDocuments/programs/pmf/runs';
% $$$ particles = {'532_091122028_p1','532_091121064_p1',...
% $$$              '532_091124112_p1','532_091125014_p1',...
% $$$              '532_091124047_p1'};
particles = {'10812032_p1'};%'10813052_p1'};
getmatname = inline('regexprep(x,''(.+)\_p[0-9]+$'',''$1.mat'')','x');

for i=1:length(particles),
    mf = getmatname(particles{i});
    load(fullfile(matdir,mf));
    for j=1:length(S.particles);
        pname = S.particles(j).name;        
        if ~any(strmatch(pname,particles)), continue; end;
        disp(pname);
        npixels = size(S.stackarr,1)*size(S.stackarr,2);
% $$$         nlevels = size(S.stackarr,3);
        %%
        stackarr = reshape(S.stackarr,npixels,nlevels);
        [X,Y] = meshgrid(S.xcoord,S.ycoord);        
        xx = reshape(X,npixels,1);
        yy = reshape(Y,npixels,1);
        bw = find(reshape(S.particles(j).bw,npixels,1));
        %%
        ve = find(all(~isnan(stackarr(bw,:)))); % valid energies
        nlevels = length(ve);
        %%
        mkdir(fullfile(exportpath,pname));
        %%
        fid = fopen(fullfile(exportpath,pname,'samples.txt'),'w');
        fprintf(fid,'%f,%f\n',[xx(bw), yy(bw)]');
        fclose(fid);
        %%
        fid = fopen(fullfile(exportpath,pname,'variables.txt'),'w');
        fprintf(fid,'%f\n',S.energy(ve)');
        fclose(fid);
        %%
        fid = fopen(fullfile(exportpath,pname,'matrix.dat'),'w');
        fprintf(fid,strcat(repmat('%f\t',1,nlevels-1),'%f\n'),...
                stackarr(bw,ve)');
        fclose(fid);
        %%
        fid = fopen(fullfile(exportpath,pname,'std_dev.dat'),'w');
        fprintf(fid,strcat(repmat('%f\t',1,nlevels-1),'%f\n'),...
                repmat(1,length(bw),nlevels)');
        fclose(fid);
    end
end
