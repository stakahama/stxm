%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~export_diamspectra.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

% $$$ exports diameter and spectra in spectra.txt

clear; close;
run headr;
run userinputs;

run getstacklist; % creates variable 'stacks' in global space
matfiles = stacks; clear stacks;
fid = fopen(fullfile(outdir,'spectra.txt'),fperm);
count = 0;
for i=1:length(matfiles),
    if strcmp(matfiles(i).name(1),'.')~=0, continue; end
    disp(sprintf('%d: %s',i,matfiles(i).name));
    load(fullfile(matdir,matfiles(i).name));
    nvar = length(S.energy);
    count = count + 1;
    if count == 1,
        fprintf(fid,'Particle\tDiameter\t');
        fprintf(fid,strcat(repmat('%.02f\t',1,nvar-1),'%.02f\n'),S.energy);
    end
    for j=1:length(S.particles),
        fprintf(fid,'%s\t',S.particles(j).name);    
        fprintf(fid,'%.03f\t',S.particles(j).diameter);
        fprintf(fid,strcat(repmat('%.03f\t',1,nvar-1),'%.03f\n'),...
                S.particles(j).avespec);
    end
end
fclose(fid);

