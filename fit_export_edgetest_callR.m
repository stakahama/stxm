%% Must have R installed.
%% Run this script as 
%%   'sudo /Users/username/bin/mscript fit_export_edgetest_callR.m'
%%   or else the R libraries do not load

clear; close all;
run headr;
run userinputs;

run getstacklist; % creates variable 'stacks' in global space

coeffnames = {'bg', 'totc', 'bzqC=C', 'C=C', 'C=O', 'C-H', 'COOH', 'CNH-COH', 'CO3', ...
 'sigma1', 'sigma2', 'sigma1w', 'sigma2w', 'arctan', 'K'};

rcmd = ['sudo '...
        'Rscript --arch=x86_64 --no-save edgefitting/fitedgesLMsubset.r '...
        '%s %s'];
% $$$ rcmd = ['sudo '...
% $$$         'R --arch x86_64 CMD BATCH --no-save edgefitting/fitedgesLMsubset.r ' ...
% $$$         '%s %s '...
% $$$         '/dev/tty'];

for feature=edgefeature,
    cname = regexprep(coeffnames{feature},'[-=]','');
    folder = sprintf(fullfile(outdir,'edgefits_%s'),cname);
    mkdir(folder);
    fout = fopen(fullfile(outdir,sprintf('edgeBIC_%s.txt',cname)),'w');
    fprintf(fout,'particle\tR\tRstar\tbulk\tsurf\tBIClayered\tBICmean\tfitstatus\n');
    for i=1:length(stacks);
        if isempty(strfind(stacks(i).name,'.mat')), continue; end
        try,
            load(fullfile(matdir,stacks(i).name));
            for j=1:length(S.particles),
                s = addfds(S.particles(j),S,{'energy','xcoord','ycoord','ccoeffs'});
                s.bw = imfill(bwmorph(s.bw,'close'),'holes');
                [x,y,bw,r] = getedges(s,7);
                filename = strcat(S.particles(j).name, '_edge.txt');
                fid = fopen(filename,'w');
                fprintf(fid,'%.05f\n',r);
                fprintf(fid,'%.05f\t%.05f\t%d\n',[x,y(:,1),bw]');
                fclose(fid);
                [status,output] = ...
                    system(sprintf(rcmd,... %[rcmd '%s %s'],...
                                   filename,...
                                   fullfile(folder, strrep(filename, ...
                                                           '.txt','.png'))));
                if length(output)>0,
                    fprintf(fout,sprintf('%s\t%d\n',...
                                         output(1:end-1), ...
                                         status));
                else,
                    fprintf(fout,sprintf(['%s\t' '%s' '%d\n'],...
                                         S.particles(j).name,...
                                         repmat('NaN\t',1,6),...
                                         status));
                end
                disp(output);
                delete(filename);
            end
        end
    end
    fclose(fout);    
end

