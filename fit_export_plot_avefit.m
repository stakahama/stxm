%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~export_avefit.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

% $$$ fits peaks to spectra; exports fit parameters and figures
% $$$     resaves image object with fit parameters
% $$$ see loadCfunctions for alternate fitting routines; by default 
% $$$     method 4 (with potassium) is used (hence the name, meth4K)

clear; close;
run headr;
run userinputs;

run getstacklist; % creates variable 'stacks' in global space

cfun = loadCfunctions;
plotfun = plottingfunctions;
load RColors;
options = optimset('lsqcurvefit');
options = optimset(options,'MaxFunEval',750*16);

outpath = fullfile(outdir,'meth4Kfits');
mkdir(outpath)

suffix = {'_avespec','_Kintr'};
meth = fitmethod; 
fid = fopen(fullfile(outdir,sprintf('fitpars_meth%dK.txt',meth)),fperm);
count = 0;
for iter = {stacks.name},
    stackname = strrep(char(iter),'.mat','');
    
    if ~isempty(strfind(stackname,'.')), continue; end
    load(fullfile(matdir,sprintf('%s.mat',stackname)));

    for j = 1:length(S.particles),
        if ~overwrite & ...
                exist(strcat(fullfile(outpath,S.particles(j).name),suffix{1},...
                             '.png'),'file'),
            continue;
        end
        disp(S.particles(j).name);
        if isempty(strmatch('aveccoeffs',fieldnames(S.particles(j)))),       
            disp('fit')
            
            %% fit peaks
            spi = addfds(S.particles(j),S,{'energy'});
            try,
                spi = feval(cfun.Cavefit,spi,meth);
                spi = feval(cfun.integrpotassium,spi,meth);      
            catch,
                continue
            end
            for fdnm = fieldnames(spi)',
                fd = char(fdnm);
                if isempty(strmatch(fd,fieldnames(S.particles(j)))),
                    S.particles(j).(fd) = spi.(fd);
                elseif isempty(S.particles(j).(fd)),
                    S.particles(j).(fd) = spi.(fd);          
                end
            end
            
            S.particles = rmfield(S.particles,'energy');        
            save(fullfile(matdir,stackname),'S');    
            
        end

        %% plot
        try,
            feval(plotfun.avespecplot,...
                  addfds(S.particles(j),S,{'energy'}),meth,...
                  outpath,suffix{1});
            feval(plotfun.Kplots,addfds(S.particles(j),S,{'energy'}), ...
                  outpath,suffix{2}); 
        end
        
        %% write
        if count == 0,
            fheadr = [S.particles(j).coeffnames,{'K'}];
            for z=1:length(fheadr),
                if z==length(fheadr), sep='\n'; else sep='\t'; end
                fprintf(fid,strcat('%s',sep),fheadr{z});
            end
        end    
        fprintf(fid,'%s\t',S.particles(j).name);        
        fprintf(fid,strcat(repmat('%f\t',1,...
                                  length(S.particles(j).aveccoeffs)),'%f\n'),...
                [S.particles(j).aveccoeffs,S.particles(j).kval]); 
        count = count + 1;    
    end
end
fclose(fid);
close all;
