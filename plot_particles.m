%%%%%%%%%%%%%%%%%%%%
% Code for processing STXM data from beamline 5.3.2.
% ~showparticles.m~
% $Rev: 17 $
% Sept. 2009
% Satoshi Takahama (stakahama@ucsd.edu)
%%%%%%%%%%%%%%%%%%%%

clear; close;
run headr;
run userinputs;

outdir = fullfile(outdir,'images');
mkdir(outdir)
suffix = '_particle';

plotfun = plottingfunctions;
run getstacklist; % creates variable 'stacks' in global space
matfiles = stacks;
clear stacks;

for i=1:length(matfiles),
    if( strcmp('.',matfiles(i).name(1))~=0 ) continue; end
    stackname = strrep(matfiles(i).name,'.mat','');
    disp(sprintf('%d: %s',i,matfiles(i).name));
    load(fullfile(matdir,matfiles(i).name));
    for j=1:length(S.particles),
        if ~overwrite & ...
                exist(fullfile(outdir,strcat(S.particles(j).name,...
                                             suffix,'.png')),'file'),
            continue;
        end
        s = addfds(S.particles(j),S,{'xcoord','ycoord',...
                            'energy','stackarr','totalc'});    
        feval(plotfun.physmorphimages,s,outdir,suffix);
    end
end
