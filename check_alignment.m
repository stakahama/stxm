

run headr;
% $$$ load('matfiles/10813052.mat')
load('matfiles/10812032.mat')

% Use C-c C-c to break out of loop

close all;
figure
m = 64;
graymap = gray(m);
colormap(graymap(m:-1:1,:));
for k = 1:size(S.stackarr,3),
    imh = imagesc(S.xcoord,S.ycoord,S.stackarr(:,:,k));
    set(gca,'ydir','normal');
    set(gca,'dataaspectratio',[1,1,1]);
    xlabel('X position (\mum)') 
    ylabel('Y position (\mum)')     
    title(sprintf('Absolute Abs. at %.02f',S.energy(k)));
    drawnow
    pause(.1)
end

% $$$ colormap(jet)
% $$$ myimagesc(S,S.totalc);